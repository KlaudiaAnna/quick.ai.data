﻿using QuickAI.ImportData.PrepareDataSet.BLL.GeneralMethods;
using QuickAI.ImportData.PrepareDataSet.BLL.Log;
using QuickAI.ImportData.PrepareDataSet.DAL;
using QuickAI.ImportData.PrepareDataSet.Interfaces.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
namespace QuickAI.ImportData.PrepareDataSet.Service
{
    [ServiceBehavior(AddressFilterMode = AddressFilterMode.Any)]

    public class ObtainDataService : IObtainData, IObtainData2, IDataMongo, IDataFromMongo
    {
        LogtoFile logtoFile = new LogtoFile();
        private List<TweetCollectionCompleted> listT;
        public List<TweetCollectionCompleted> listS, rlist;
        public bool GetDataSet()
        {
            try
            {
                GenerateDataSets dataSet = new GenerateDataSets();
                bool success = dataSet.GetDataSet();
                return success;
            }
            catch (Exception ex)
            {
                logtoFile.LogMessagePrint(ex.Message);
                return false;
            }
        }
        public List<TweetCollectionCompleted> GetCollectionComplete()
        {
            try
            {
                TwitterGenerateData twitterGenerateData = new TwitterGenerateData();
                listT = twitterGenerateData.GetCollectionComplete();
                var idSTock = ReadData();
                StockTwitsGenerateData stockTwitsGenerateData = new StockTwitsGenerateData(idSTock);
                listS = stockTwitsGenerateData.GetCollectionComplete();
                if (listT == null || listS == null)
                {
                    logtoFile.LogMessagePrint("Stocktwits and Twitter returned 0 elements!");
                    return null;
                }

                var listy = listS.Concat(listT).ToList();
                return listy;
            }

            catch (Exception ex)
            {
                logtoFile.LogMessagePrint(ex.Message);
                return null;
            }
        }
        public bool SetData()
        {
            try
            {
                rlist = GetCollectionComplete();
                DataToDb db = new DataToDb(rlist);
                var resultData = db.SetData();
                return resultData;
            }
            catch (Exception ex)
            {
                logtoFile.LogMessagePrint(ex.Message);
                return false;
            }
        }
        public long ReadData()
        {
            try
            {
                ReadDataDatabase dataDatabase = new ReadDataDatabase();
                return dataDatabase.ReadData();
            }
            catch (Exception ex)
            {
                logtoFile.LogMessagePrint(ex.Message);
                return 0;
            }
        }

        public List<TickersList> TickerlistList()
        {
            try
            {
                GenerateDataSets sets = new GenerateDataSets();
                var tickers = sets.TickerlistList();
                return tickers;
            }
            catch (Exception e)
            {
                logtoFile.LogMessagePrint(e.Message);
                return null;
            }
        }
    }
}

