﻿using System;
using System.Configuration;
using System.IO;
using System.Web;
using System.Web.Hosting;
namespace QuickAI.ImportData.PrepareDataSet.BLL
{
    public class SetPath
    {
        protected string path = ConfigurationManager.AppSettings["Path"];
        protected string pathLog = ConfigurationManager.AppSettings["PathtoLog"];
        protected string NewPath;
        protected SetPath(string setpath)
        {
            if (string.IsNullOrEmpty(setpath))
            {
                throw new ArgumentException("message", nameof(setpath));
            }
            NewPath = ConfigurationManager.AppSettings[setpath];
        }
        public string SetPathToFile(string fileName)
        {
            string real = HttpContext.Current.Server.MapPath(NewPath);
            string root = HostingEnvironment.ApplicationPhysicalPath;
            string someFile = Path.Combine(Environment.CurrentDirectory, real, root);
            return path;
        }
    }
}

