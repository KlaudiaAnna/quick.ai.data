﻿using System;
using System.Linq;
using System.Text.RegularExpressions;
using QuickAI.ImportData.PrepareDataSet.Interfaces.StockTwitsModel;
namespace QuickAI.ImportData.PrepareDataSet.BLL.MethodStockTwits
{
    public class SymbolOne
    {
        private string ticker = string.Empty;
        public SymbolOne(string ticker)
        {
            this.ticker = ticker;
        }
        public void CheckSymbol(StockCollection el)
        {
            for (int i = el.Stockdata.Count - 1; i >= 0; i--)
            {
                char s = '$';
                string Symbl = String.Concat(s, ticker);

                //(?<=\$)[a-zA-Z]+

                string text = el.Stockdata[i].Body;
                bool value = text.Contains(Symbl);

                var regexp = new Regex(@"(?<=/$)[a-zA-Z]+");
 
                if (value == true)
                {
                    var df = text.Replace(Symbl, "*****");
                    bool value2 = df.Contains(s);
                    var matches = regexp.Matches(df);
                    if (value2 == true ) 
                    {
                        el.Stockdata.RemoveAt(i);
                    }
                    else
                    {
                        df.Replace("*****", Symbl);
                    }
                }
            }
        }
    }
}
