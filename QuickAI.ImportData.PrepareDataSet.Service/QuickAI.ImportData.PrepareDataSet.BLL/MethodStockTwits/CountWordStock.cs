﻿using System;
using QuickAI.ImportData.PrepareDataSet.Interfaces.StockTwitsModel;
using QuickIA.ImportData.PrepareDataSet.BLL.Configuration;
namespace QuickAI.ImportData.PrepareDataSet.BLL.MethodStockTwits
{
    public static class CountWordStock
    {
        public static void CountWordStocker(StockCollection el)
        {
            var counter = Configuration.GetApplicationConfig().CounterWords;

            for (int elem = el.Stockdata.Count - 1; elem >= 0; elem--)
            {
                string[] sep = el.Stockdata[elem].Body.Split(' ');
                var countwordintweet = el.Stockdata[elem].Body.Split(sep, StringSplitOptions.RemoveEmptyEntries).Length;
                if (counter > countwordintweet)
                {
                    el.Stockdata.RemoveAt(elem);
                }
            }
        }
    }
}