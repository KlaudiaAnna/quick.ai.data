﻿using System;
using System.Text;
using System.Net;
using QuickAI.ImportData.PrepareDataSet.BLL.Log;
using QuickIA.ImportData.PrepareDataSet.BLL.Configuration;
namespace QuickAI.ImportData.PrepareDataSet.BLL.Download
{
    public class QuickIaWebRequest
    {
        private string ticker = string.Empty;
        private string function = string.Empty;

        public QuickIaWebRequest(string ticker, string function)
        {
            this.ticker = ticker;
            this.function = function;
        }
        public string Download()
        {
            try
            {
                LogtoFile logtoFile = new LogtoFile();
                logtoFile.LogMessagePrint(string.Format("Start download for ticker {0} and function {1}", ticker, function));

                bool doWork = true;
                string output = string.Empty;
                int counter = 0;

                while (doWork)
                {
                    if (counter > 6)
                        return output;
                    string uri = Url + "&apikey=" + Configuration.GetApplicationConfig().RandomApiKey;

                    logtoFile.LogMessagePrint("Download data");

                    var request = (HttpWebRequest)WebRequest.Create(uri);
                    request.Method = "Get";
                    output = string.Empty;

                    using (var response = request.GetResponse())
                    {
                        logtoFile.LogMessagePrint("Get response");
                        using (var stream = new System.IO.StreamReader(response.GetResponseStream(), Encoding.GetEncoding(1252)))
                        {
                            output = stream.ReadToEnd();
                        }
                    }
                    if (output.Contains("Thank you for using Alpha Vantage!"))
                    {
                        logtoFile.LogMessagePrint(string.Format("output contains {0}", output.Contains("Thank you for using Alpha Vantage!")));

                        System.Threading.Thread.Sleep(500);
                        doWork = true;
                        counter++;
                        logtoFile.LogMessagePrint("stop sleep");
                    }
                    else
                        doWork = false;
                }
                logtoFile.LogMessagePrint("Returned value of output");

                if (output == null)
                {
                    return null;
                }

                return output;
            }
            catch (Exception ex)
            {
                LogtoFile logtoFile = new LogtoFile();
                logtoFile.LogMessagePrint(string.Format("Exception occured while running WebRequest: {0}, {1}",
                ex.Message, ex.InnerException));
                return null;
            }
        }
        public string url;
        public string Url
        {
            get
            {
                if (string.IsNullOrEmpty(url))
                    url = Configuration.GetApplicationConfig().ApiUrl + function + "&symbol=" + ticker + "&outputsize=full";
                return url;
            }
            set
            {
                url = value;
            }
        }
    }
}










