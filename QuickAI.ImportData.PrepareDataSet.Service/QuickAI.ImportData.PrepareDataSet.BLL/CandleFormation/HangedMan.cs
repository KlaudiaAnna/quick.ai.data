﻿namespace QuickAI.ImportData.PrepareDataSet.BLL.CandleFormation
{
    /*-0.8*/
    /**
      the opposite of hammer 
         **/
    class HangedMan : FormationBase
    {
        protected const decimal PERCENTAGE = 0.7M;

        public HangedMan(Context c)
        {
            context = c;
        }
        public void ProcessHangedMan()
        {
            for (int i = 0; i < context.Elements.Count; i++)
            {
                if (IsHossa(i))
                {
                    var e = context.Elements[i];
                    decimal open = e.Open;
                    decimal high = e.High;
                    decimal low = e.Low;
                    decimal close = e.Close;

                    decimal diff = high - low;
                    decimal percentageOfTheDifference = diff * PERCENTAGE;

                    decimal threshold = low + percentageOfTheDifference;

                    if (open > threshold && close > threshold)
                    {
                        e.Values = string.Concat(e.Values, ",", "-0.8");
                    }
                    else
                    {
                        e.Values = string.Concat(e.Values, ",", "0");
                    }
                }
                else
                {
                    var e = context.Elements[i];
                    e.Values = string.Concat(e.Values, ",", "0");
                }
            }

        }
    }
}