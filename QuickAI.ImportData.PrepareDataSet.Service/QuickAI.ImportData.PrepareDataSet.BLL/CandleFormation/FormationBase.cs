﻿namespace QuickAI.ImportData.PrepareDataSet.BLL.CandleFormation
{
    public class FormationBase
    {
        protected const int HOSSA_BESSA_LENGHT = 10;
        public Context context;
        public bool IsHossa(int i)      //wzrostowy
        {
            if ((i + HOSSA_BESSA_LENGHT) >= context.Elements.Count - 1)
            {
                return false;
            }
            var currentClose = context.Elements[i].Close;
            var previoustClose = context.Elements[i + HOSSA_BESSA_LENGHT].Close;
            if (previoustClose < currentClose)
            {
                return true;
            }
                return false;
        }
        public bool IsBessa(int i)  //spadkowy
        {
            return !IsHossa(i);
        }
    }
}