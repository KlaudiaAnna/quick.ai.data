﻿namespace QuickAI.ImportData.PrepareDataSet.BLL.CandleFormation
{
    public class Hammer : FormationBase
    {
        protected const decimal PERCENTAGE = 0.7M;
        /* 0.8 else 0 */

        public Hammer(Context c)
        {
            context = c;
        }
        public void ProcessHammer()
        {
            for (int i = 0; i < context.Elements.Count; i++)
            {
                if (IsBessa(i))
                {
                    var e = context.Elements[i];
                    decimal open = e.Open;
                    decimal high = e.High;
                    decimal low = e.Low;
                    decimal close = e.Close;

                    decimal diff = high - low;
                    decimal percentageOfTheDifference = diff * PERCENTAGE;

                    decimal threshold = low + percentageOfTheDifference;

                    if (open > threshold && close > threshold)
                    {
                        e.Values = string.Concat(e.Values, ",", "0.8");
                    }
                    else
                    {
                        e.Values = string.Concat(e.Values, ",", "0");
                    }
                }
                else
                {
                    var e = context.Elements[i];
                    e.Values = string.Concat(e.Values, ",", "0");
                }
            }
        }
    }
}





