﻿using QuickIA.ImportData.PrepareDataSet.BLL;

namespace QuickAI.ImportData.PrepareDataSet.BLL.CandleFormation
{   /* 0.9 */
    class Hossa_h : FormationBase
    {
        public Hossa_h(Context c)
        {
            context = c;
        }
        /*
        Objęcie hossy: 0.9
       Trend spadkowy.
       Poprzedni dzień swieca spadkowa. Open > Close.
       Nastepnego dnia:
       (Open < close z poprzedniego dnia) && (close > open z popdzedniego dnia)
        */
        public void Process_Hossa_h() // trend spadkowy
        {
            for (int i = 0; i < context.Elements.Count; i++)
            {
                if (IsHossa(i))
                {
                    if ((context.Elements[i].Open > context.Elements[i].Close) &&
                    (context.Elements[i + 1].Open < context.Elements[i].Close) &&
                    (context.Elements[i + 1].Close > context.Elements[i].Open))
                    {
                        var e = context.Elements[i];
                        e.Values = string.Concat(e.Values, ",", "0.9");
                    }
                    else
                    {
                        var e = context.Elements[i];
                        e.Values = string.Concat(e.Values, ",", "0");
                    }
                }
                else
                {
                    var e = context.Elements[i];
                    e.Values = string.Concat(e.Values, ",", "0");
                }
            }
        }
    }
    class Bessa_b : FormationBase
    {
        public Bessa_b(Context c)
        {
            context = c;
        }
        public void Process_Bessa_b() // trend wzrostowy
        {
            for (int i = 0; i < context.Elements.Count; i++)
            {
                if (i == context.Elements.Count - 1)
                {
                    var e = context.Elements[i];
                    e.Values = string.Concat(e.Values, ",", "0");
                    return;
                }
                if (IsBessa(i))
                {
                    // poprzedni dzien open > close 
                    if ((context.Elements[i].Open < context.Elements[i].Close) &&
                        (context.Elements[i + 1].Open > context.Elements[i].Close) &&
                        (context.Elements[i + 1].Close < context.Elements[i].Open))
                    {
                        var e = context.Elements[i];
                        e.Values = string.Concat(e.Values, ",", "-0.9");
                    }
                    else
                    {
                        var e = context.Elements[i];
                        e.Values = string.Concat(e.Values, ",", "0");
                    }
                }
                else
                {
                    var e = context.Elements[i];
                    e.Values = string.Concat(e.Values, ",", "0");
                }
            }
        }
    }
}
