﻿namespace QuickAI.ImportData.PrepareDataSet.BLL.CandleFormation
{   /* 1 */
    class MorningStar : FormationBase
    {
        public MorningStar(Context c)
        {
            context = c;
        }
        public void ProcessMorningStar()
        {
            for (int i = 0; i < context.Elements.Count; i++)
            {
                //  if (IsBessa(i))
                //{  
                if ((i == 0) || (i == context.Elements.Count - 1))
                {
                    var el = context.Elements[i];
                    el.Values = string.Concat(el.Values, ",", "0");
                }
                else
                {
                    var elem = context.Elements[i];
                    var data = elem.Data;
                    decimal candle1_open = context.Elements[i + 1].Open;
                    decimal candle1_close = context.Elements[i + 1].Close;
                    decimal candle3_close = context.Elements[i - 1].Close;
                    decimal candle3_open = context.Elements[i - 1].Open;
                    decimal candle2_open = context.Elements[i].Open;
                    decimal candle2_close = context.Elements[i].Close;

                    if ((candle1_open > candle1_close) && (candle3_open < candle3_close) // czy pierwsza swieca jest spadkowa a trzecia wzrostowa
                        && (candle1_open > candle2_close) && (candle2_close < candle3_open)) // luka pomiedzy pierwsza a druga druga  atrzecia

                    {
                        elem.Values = string.Concat(elem.Values, ",", "1");
                    }
                    else
                    {
                        elem.Values = string.Concat(elem.Values, ",", "0");
                    }
                }
            }
        }
    }
}
