﻿using QuickAI.ImportData.PrepareDataSet.BLL.Log;
using QuickAI.ImportData.PrepareDataSet.Interfaces.Interfaces;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
namespace QuickAI.ImportData.PrepareDataSet.BLL
{
    public class Context : SetPath
    {
        public Context() : base("Path")
        {
            Elements = new List<ContextElement>();
            NewPath = ConfigurationManager.AppSettings["Path"];
        }
        LogtoFile logtoFile = new LogtoFile();
        internal static IEnumerable<object> ContextElement;
        public List<ContextElement> Elements { get; set; }
        public ContextElement GetElementByDate(string date)
        {
            foreach (var item in Elements)
            {
                if (item.Data == date)
                    return item;
            }
            return null;
        }
        public void SaveToFile(string fileName) 
        {
           logtoFile.LogMessagePrint(string.Format("Save to file {0}", fileName));
           fileName = string.Concat(fileName, ".csv"); // file name + extension
           string name = SetPathToFile(fileName);
          
            if (File.Exists(name))
            {
                File.Delete(name);
            }

            foreach (var e in Elements)
            {
                var lines = e.Values.Split(',').Count();
                //    if (lines < 58) continue;  // compare numbers of values
                File.AppendAllText(name, e.Values);
                File.AppendAllText(name, Environment.NewLine);
            }
        }
        public void Save_Classifier(string fileC)
        {
            logtoFile.LogMessagePrint(string.Format("Save to file classifer {0}", fileC));

            fileC = string.Concat(fileC, "_classifier", ".csv"); // file name + extension
            string name = SetPathToFile(fileC);

            if (File.Exists(name))
            {
                File.Delete(name);
            }
            for (int i = 0; i < Elements.Count; i++)
            {
                if (i == Elements.Count - 1)
                {

                    if (Elements[i].Values.Split(',').Count() < 58) continue;
                    File.AppendAllText(name, "," + "0");
                    return;
                }
                decimal currClose = Elements[i].Close;
                decimal prevClose = Elements[i + 1].Close;

                if (i >= 0)
                {
                    if (prevClose > currClose)
                    {
                        if (Elements[i].Values.Split(',').Count() < 58) continue;
                        Elements[i].Values = string.Concat(Elements[i].Values, ",", "1", Environment.NewLine);
                    }
                    else
                    {
                        if (Elements[i].Values.Split(',').Count() < 58) continue;
                        Elements[i].Values = string.Concat(Elements[i].Values, ",", "0", Environment.NewLine);
                    }
                }
                if (Elements[i].Values.Split(',').Count() < 58) continue;
                File.AppendAllText(name, Elements[i].Values);
            }
        }
        public void Save_Reversed(string fileR)
        {
            var sorted = Elements
                .SelectMany(a => a.Data)
                .OrderBy(a => a)
                .Distinct()
                .ToList();
            logtoFile.LogMessagePrint(string.Format("Save to file reversed {0}", fileR));
            Elements = Elements.OrderBy(a => a.Data).ToList();
            fileR = string.Concat(fileR, "_reserved", ".csv");
            string name = SetPathToFile(fileR);

            if (File.Exists(name))
            {
                File.Delete(name);
            }
            foreach (var e in Elements)
            {
                var lines = e.Values.Split(',').Count();
                if (lines < 58) continue;
                File.AppendAllText(name, e.Values);
                File.AppendAllText(name, Environment.NewLine);
            }
        }
    }
}

