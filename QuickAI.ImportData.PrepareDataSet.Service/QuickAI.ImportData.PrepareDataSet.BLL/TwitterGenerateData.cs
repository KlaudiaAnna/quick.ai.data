﻿using System;
using QuickAI.ImportData.PrepareDataSet.BLL.Log;
using QuickIA.ImportData.PrepareDataSet.BLL.Configuration;
using QuickAI.ImportData.PrepareDataSet.BLL.Download;
using Newtonsoft.Json;
using QuickAI.ImportData.PrepareDataSet.Interfaces.TwitterModel;
using QuickAI.ImportData.PrepareDataSet.BLL.MethodTwitter;
using MongoDB.Driver;
using QuickAI.ImportData.PrepareDataSet.BLL.Properties;
using System.Collections.Generic;
using QuickAI.ImportData.PrepareDataSet.Interfaces;
using System.Linq;

namespace QuickAI.ImportData.PrepareDataSet.BLL
{
    public class TwitterGenerateData
    {
        public List<Tweet> InsertData()
        {
            try
            {
                LogtoFile logtoFile = new LogtoFile();
                logtoFile.LogMessagePrint("started");
                ApplicationConfiguration cfg = Configuration.GetApplicationConfig();
                foreach (TickerInfo ticker in cfg.Tickers)
                {
                    Console.WriteLine("Download for " + ticker.Name);
                    RequestToTwitter p = new RequestToTwitter(ticker.Name);
                    string download = p.Download();
                    Tweetdata tw = JsonConvert.DeserializeObject<Tweetdata>(download);
                    CountWordinTweet.Words(tw);
                    Contains c = new Contains(ticker.Name);
                    c.TickerOnlyOne(tw);
                    var list = tw.Statuses;

                    return list;
                }
                return null;
            }
            catch (Exception ex)
            {
                LogtoFile logtoFile = new LogtoFile();
                logtoFile.LogMessagePrint(ex.Message);
                return null;
            }
        }
    }
}
