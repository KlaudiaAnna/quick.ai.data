﻿using QuickAI.ImportData.PrepareDataSet.BLL;
using QuickAI.ImportData.PrepareDataSet.BLL.Serized;
using System.IO;
using System.Web.Hosting;
using System.Xml;

namespace QuickIA.ImportData.PrepareDataSet.BLL.Configuration
{
    public static class Configuration
    {
        static ApplicationConfiguration config;

        public static ApplicationConfiguration GetApplicationConfig()
        {
            if (config == null)
            {
                string configurationXml;
                // get acess to congig.xml from folder App_Data 
                string XMLConfig = Path.Combine(HostingEnvironment.ApplicationPhysicalPath, "App_Data", "Config.xml"); 

                XmlDocument document = new XmlDocument();
                document.Load(XMLConfig);
                configurationXml = document.OuterXml;
                config = Utils.Deserialize<ApplicationConfiguration>(configurationXml);
            }
            return config;
        }
    }
}
