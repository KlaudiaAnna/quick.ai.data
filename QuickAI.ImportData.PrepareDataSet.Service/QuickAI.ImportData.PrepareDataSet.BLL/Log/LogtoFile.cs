﻿using System;
using System.Configuration;
using System.IO;
namespace QuickAI.ImportData.PrepareDataSet.BLL.Log
{
   public  class LogtoFile : SetPath
    {
        public LogtoFile(): base("PathtoLog")
        {
            NewPath = ConfigurationManager.AppSettings["PathtoLog"];
        }
        public void LogMessagePrint(string message)
        {
            try
            {
                StreamWriter sw = new StreamWriter(NewPath + "log_" + DateTime.Now.ToShortDateString() + "_service.txt", true);
                sw.WriteLine(DateTime.Now + " " + message);
                sw.Close();
            }
            catch { LogMessageError(message); }
        }
        public void LogMessageError(string message)
        {
            try
            {
                string name = SetPathToFile("log_path");       
                StreamWriter sw = new StreamWriter(name + "log_" + DateTime.Now.ToShortDateString() + "_Catch.txt", true);
                sw.WriteLine(DateTime.Now + " " + message);
                sw.Close();
            }
            catch { }
        }
    }
}
