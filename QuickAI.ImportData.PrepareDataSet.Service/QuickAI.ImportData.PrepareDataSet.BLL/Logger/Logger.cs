﻿using System;

namespace QuickIA.ImportData.PrepareDataSet.BLL.Logger
{
    public static class Logger
    {
        static bool isDebug = true;

        public static void LogMessage(string message)
        {
            if (isDebug)
                Console.WriteLine(message);
        }
    }
}
