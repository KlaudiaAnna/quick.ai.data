﻿using System;
using System.Collections.Generic;
using QuickAI.ImportData.PrepareDataSet.BLL.Download;
using QuickAI.ImportData.PrepareDataSet.BLL.Log;
using QuickAI.ImportData.PrepareDataSet.BLL.MethodStockTwits;
using QuickAI.ImportData.PrepareDataSet.Interfaces.Interfaces;
using QuickIA.ImportData.PrepareDataSet.BLL.Configuration;
namespace QuickAI.ImportData.PrepareDataSet.BLL.GeneralMethods
{
    public class StockTwitsGenerateData : IObtainData2
    {
        private readonly long _idSTock;
        public StockTwitsGenerateData(long idSTock)
        {
            _idSTock = idSTock;
        }
        public List<TweetCollectionCompleted> GetCollectionComplete()
        {
            try
            {
                List<TweetCollectionCompleted> objecList = new List<TweetCollectionCompleted>();
                var configuration = Configuration.GetApplicationConfig();
                foreach (var ticker in configuration.Tickers)
                {
                    RequestToStocktwits request = new RequestToStocktwits(ticker.Name, _idSTock);
                    var respo = request.Request();
                    CountWordStock.CountWordStocker(respo);
                    SymbolOne symbol = new SymbolOne(ticker.Name);
                    symbol.CheckSymbol(respo);
                    foreach (var x in respo.Stockdata)
                    {
                        objecList.Add(new TweetCollectionCompleted
                        {
                            Text = x.Body,
                            Symbol = respo.Symbol.Symbolticker,
                            UserName = x.User.NameUser,
                            IdUser = x.User.IdUser,
                            Date = x.Created_at,
                            IdTweet = x.Idbody,
                            DateInsertedDb = DateTime.Today.ToShortDateString(),
                            Human_Rating = String.Empty,
                            Neural_Rating = String.Empty,
                            Portal = "Stocktwits"
                        });
                    }
                }
                 return objecList; 
            }
            catch (Exception e)
            {
                LogtoFile logtoFile = new LogtoFile();
                logtoFile.LogMessagePrint(e.Message);
                return null ;
            }
        }
    }
}
