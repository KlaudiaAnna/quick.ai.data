﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Net;
using Newtonsoft.Json;
using QuickAI.ImportData.PrepareDataSet.BLL.Download;
using QuickAI.ImportData.PrepareDataSet.BLL.Log;
using QuickAI.ImportData.PrepareDataSet.BLL.MethodTwitter;
using QuickAI.ImportData.PrepareDataSet.Interfaces.Interfaces;
using QuickAI.ImportData.PrepareDataSet.Interfaces.TwitterModel;
namespace QuickAI.ImportData.PrepareDataSet.BLL.GeneralMethods
{
    public class TwitterGenerateData : IObtainData2
    {
        public List<TweetCollectionCompleted> GetCollectionComplete()
        {
            List<TweetCollectionCompleted> obj = new List<TweetCollectionCompleted>();
            var logtoFile = new LogtoFile();
            var cfg = QuickIA.ImportData.PrepareDataSet.BLL.Configuration.Configuration.GetApplicationConfig();
            try
            {
                logtoFile.LogMessagePrint("started");
                foreach (var ticker in cfg.Tickers)
                {
                    Console.WriteLine("Download for " + ticker.Name);
                    var p = new RequestToTwitter(ticker.Name);
                    var download = p.Download();
                    string decode = WebUtility.HtmlDecode(download);
                    Tweetdata tw = JsonConvert.DeserializeObject<Tweetdata>(decode);

                    CheckStatusofTweet.CheckStatus(tw.Statuses);
                    SaveTweets saveTweets = new SaveTweets();
                 //   saveTweets.SavetoTxt(tw.Statuses, "Twitter", ticker.Name);
                    CountWordinTweet.Words(tw);
                    var c = new Contains(ticker.Name);
                    c.TickerOnlyOne(tw);
                    var list = tw.Statuses;
                    foreach (var x in list)
                    {
                        DateTime s = DateTime.ParseExact(x.Created, "ddd MMM dd HH:mm:ss zzz yyyy", CultureInfo.InvariantCulture);

                        obj.Add(new TweetCollectionCompleted
                        {
                            Date = s,
                            IdUser = x.User.IDuser,
                            UserName = x.User.Name,
                            IdTweet = x.Id_str,
                            Text = x.Text,
                            Symbol = ticker.Name,
                            DateInsertedDb = DateTime.Today.ToShortDateString(),
                            Human_Rating = String.Empty,
                            Neural_Rating = String.Empty,
                            Portal = "Twitter"
                        });
                    }
                }
                return obj;
            }
            catch (Exception ex)
            {
                logtoFile = new LogtoFile();
                logtoFile.LogMessagePrint(ex.Message);
                return null;
            }
        }
    }
}
