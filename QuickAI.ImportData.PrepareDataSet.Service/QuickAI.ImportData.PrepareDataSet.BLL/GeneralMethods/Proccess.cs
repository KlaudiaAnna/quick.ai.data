﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using MongoDB.Driver.Core.Events;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using QuickAI.ImportData.PrepareDataSet.BLL.CandleFormation;
using QuickAI.ImportData.PrepareDataSet.BLL.Download;
using QuickAI.ImportData.PrepareDataSet.BLL.Log;
using QuickAI.ImportData.PrepareDataSet.BLL.NormalizeValues;
using QuickAI.ImportData.PrepareDataSet.BLL.Worker;
using QuickAI.ImportData.PrepareDataSet.BLL.Worker.currency;
using QuickAI.ImportData.PrepareDataSet.Interfaces.Interfaces;
using QuickIA.ImportData.PrepareDataSet.BLL.Configuration;

namespace QuickAI.ImportData.PrepareDataSet.BLL.GeneralMethods
{
    public class ProccessData
    {
        public List<TickerInfo> failedTickers = new List<TickerInfo>();
        public List<IndicatorInfo> failedIndentificator = new List<IndicatorInfo>();
        LogtoFile logtoFile = new LogtoFile();
        public Tuple<List<TickerInfo>, List<IndicatorInfo>> ProcessData(List<TickerInfo> tickers, List<IndicatorInfo> identificators)
        {
            try
            {
                foreach (TickerInfo ticker in tickers)
                {
                    Context context = new Context();
                    QuickIaWebRequest webR = new QuickIaWebRequest(ticker.Name, "TIME_SERIES_DAILY");

                    string data = webR.Download();

                    TickerData tickerData = JsonConvert.DeserializeObject<TickerData>(data);
                    tickerData.CleanseData();

                    if (tickerData.TimeSeriesData == null)
                    {
                        failedTickers.Add(new TickerInfo { Name = ticker.Name, Index = ticker.Index });
                        continue;
                    }
                    tickerData.AddToContext(context, false);

                    /***********************************TICKER INDEX************************************************************************************/

                    webR = new QuickIaWebRequest(ticker.Index, "Time_Series_DAILY");
                    string idate = webR.Download();

                    tickerData = JsonConvert.DeserializeObject<TickerData>(idate);
                    tickerData.CleanseData();
                    if (tickerData.TimeSeriesData == null)
                    {
                        failedTickers.Add(new TickerInfo { Name = ticker.Name, Index = ticker.Index });
                        continue;
                    }

                    tickerData.AddToContext(context, true);

                    /***********************************************************Indicators*************************************************************/
                    foreach (IndicatorInfo ii in identificators)
                    {
                        webR = new QuickIaWebRequest(ii.Name, "TIME_SERIES_DAILY");
                        if (ii.Name == "SMA")
                        {
                            webR.Url = IndicatorSMA.GetUrl(ticker.Name);
                            string smaData = webR.Download();

                            IndicatorSMA smaDataIndicator = JsonConvert.DeserializeObject<IndicatorSMA>(smaData);
                            smaDataIndicator.CleanseData();

                            if (smaDataIndicator.TimeSeriesData == null)
                            {
                                failedIndentificator.Add(new IndicatorInfo { Name = ii.Name });
                                continue;
                            }

                            smaDataIndicator.AddToContext(context);
                        }

                        if (ii.Name == "AD")
                        {
                            webR.Url = IndicatorAD.GetUrl(ticker.Name);
                            string adData = webR.Download();

                            IndicatorAD ADDataIndicator = JsonConvert.DeserializeObject<IndicatorAD>(adData);
                            ADDataIndicator.CleanseData();

                            if (ADDataIndicator.TechnicalADdata == null)
                            {
                                failedIndentificator.Add(new IndicatorInfo { Name = ii.Name });
                                failedIndentificator.Add(ii);
                            }

                            ADDataIndicator.AddToContext(context);
                        }

                        if (ii.Name == "ADX")
                        {
                            webR.Url = IndicatorADX.GetUrl(ticker.Name);
                            string ADXata = webR.Download();

                            IndicatorADX ADXdataIndicator = JsonConvert.DeserializeObject<IndicatorADX>(ADXata);
                            ADXdataIndicator.CleanseData();

                            if (ADXdataIndicator.TechnicalADXdata == null)
                            {
                                failedIndentificator.Add(new IndicatorInfo { Name = ii.Name });
                                continue;
                            }

                            ADXdataIndicator.AddToContext(context);
                        }

                        if (ii.Name == "BBANDS")
                        {
                            webR.Url = IndicatorBBANDS.GetUrl(ticker.Name);
                            string BBANDSData = webR.Download();

                            IndicatorBBANDS BBANDSdataIndicator =
                                JsonConvert.DeserializeObject<IndicatorBBANDS>(BBANDSData);
                            BBANDSdataIndicator.CleanseData();

                            if (BBANDSdataIndicator.TechnicalBBANDSdata == null)
                            {
                                failedIndentificator.Add(new IndicatorInfo { Name = ii.Name });

                                continue;
                            }

                            BBANDSdataIndicator.AddToContext(context);
                        }

                        if (ii.Name == "T3")
                        {
                            webR.Url = IndicatorT3.GetUrl(ticker.Name);
                            string T3Data = webR.Download();

                            IndicatorT3 T3dataIndicator = JsonConvert.DeserializeObject<IndicatorT3>(T3Data);
                            T3dataIndicator.CleanseData();

                            if (T3dataIndicator.TechnicalT3data == null)
                            {
                                failedIndentificator.Add(new IndicatorInfo { Name = ii.Name });

                                continue;
                            }

                            T3dataIndicator.AddToContext(context);
                        }

                        if (ii.Name == "MACD")
                        {
                            webR.Url = IndicatorMACD.GetUrl(ticker.Name);
                            string MACDData = webR.Download();

                            IndicatorMACD MACDdataIndicator =
                                JsonConvert.DeserializeObject<IndicatorMACD>(MACDData);
                            MACDdataIndicator.CleanseData();

                            if (MACDdataIndicator.TechnicalMACDdata == null)
                            {
                                failedIndentificator.Add(new IndicatorInfo { Name = ii.Name });

                                continue;
                            }

                            MACDdataIndicator.AddToContext(context);
                        }

                        if (ii.Name == "OBV")
                        {
                            webR.Url = IndicatotOBV.GetUrl(ticker.Name);
                            string OBVData = webR.Download();

                            IndicatotOBV OBVdataIndicator = JsonConvert.DeserializeObject<IndicatotOBV>(OBVData);
                            OBVdataIndicator.CleanseData();

                            if (OBVdataIndicator.TechnicalOBVdata == null)
                            {
                                failedIndentificator.Add(new IndicatorInfo { Name = ii.Name });

                                continue;
                            }

                            OBVdataIndicator.AddToContext(context);
                        }

                        if (ii.Name == "STOCH")
                        {
                            webR.Url = IndicatorStoch.GetUrl(ticker.Name);
                            string StochData = webR.Download();

                            IndicatorStoch StochdataIndicator =
                                JsonConvert.DeserializeObject<IndicatorStoch>(StochData);
                            StochdataIndicator.CleanseData();

                            if (StochdataIndicator.TechnicalSTOCHdata == null)
                            {
                                failedIndentificator.Add(new IndicatorInfo { Name = ii.Name });

                                continue;
                            }

                            StochdataIndicator.AddToContext(context);
                        }

                        if (ii.Name == "RSI")
                        {
                            webR.Url = IndicatorRSI.GetUrl(ticker.Name);
                            string RSIData = webR.Download();

                            IndicatorRSI RSIdataIndicator = JsonConvert.DeserializeObject<IndicatorRSI>(RSIData);
                            RSIdataIndicator.CleanseData();

                            if (RSIdataIndicator.TechnicalRSIdata == null)
                            {
                                failedIndentificator.Add(new IndicatorInfo { Name = ii.Name });

                                continue;
                            }

                            RSIdataIndicator.AddToContext(context);
                        }

                        if (ii.Name == "EMA")
                        {
                            webR.Url = IndicatorEMA.GetUrl(ticker.Name);
                            string EMAData = webR.Download();

                            IndicatorEMA EMAdataIndicator = JsonConvert.DeserializeObject<IndicatorEMA>(EMAData);
                            EMAdataIndicator.CleanseData();

                            if (EMAdataIndicator.TechnicalEMAdata == null)
                            {
                                failedIndentificator.Add(new IndicatorInfo { Name = ii.Name });

                                continue;
                            }

                            EMAdataIndicator.AddToContext(context);
                        }

                        if (ii.Name == "CCI")
                        {
                            webR.Url = IndicatorCCI.GetUrl(ticker.Name);
                            string CCIData = webR.Download();

                            IndicatorCCI CCIdataIndicator = JsonConvert.DeserializeObject<IndicatorCCI>(CCIData);
                            CCIdataIndicator.CleanseData();

                            if (CCIdataIndicator.TechnicalCCIdata == null)
                            {
                                failedIndentificator.Add(new IndicatorInfo { Name = ii.Name });

                                continue;
                            }

                            CCIdataIndicator.AddToContext(context);
                        }

                        if (ii.Name == "WMA")
                        {
                            webR.Url = IndicatorWMA.GetUrl(ticker.Name);
                            string WMAData = webR.Download();

                            IndicatorWMA WMAdataIndicator = JsonConvert.DeserializeObject<IndicatorWMA>(WMAData);
                            WMAdataIndicator.CleanseData();

                            if (WMAdataIndicator.TechnicalWMAdata == null)
                            {
                                failedIndentificator.Add(new IndicatorInfo { Name = ii.Name });

                                continue;
                            }

                            WMAdataIndicator.AddToContext(context);
                        }

                        if (ii.Name == "AROON")
                        {
                            webR.Url = IndicatorAROON.GetUrl(ticker.Name);
                            string AroonData = webR.Download();

                            IndicatorAROON AroonDataIndicator =
                                JsonConvert.DeserializeObject<IndicatorAROON>(AroonData);
                            AroonDataIndicator.CleanseData();

                            if (AroonDataIndicator.TechnicalAROONdata == null)
                            {
                                failedIndentificator.Add(new IndicatorInfo { Name = ii.Name });

                                continue;
                            }

                            AroonDataIndicator.AddToContext(context);
                        }

                        if (ii.Name == "MFI")
                        {
                            webR.Url = IndicatorMFI.GetUrl(ticker.Name);
                            string Data = webR.Download();

                            IndicatorMFI MFIDataIndicator = JsonConvert.DeserializeObject<IndicatorMFI>(Data);
                            MFIDataIndicator.CleanseData();

                            if (MFIDataIndicator.TechnicalMFIdata == null)
                            {
                                failedIndentificator.Add(new IndicatorInfo { Name = ii.Name });

                                continue;
                            }

                            MFIDataIndicator.AddToContext(context);
                        }

                        if (ii.Name == "ULTOSC")
                        {
                            webR.Url = IndicatorULTOSC.GetUrl(ticker.Name);
                            string Data = webR.Download();

                            IndicatorULTOSC ULTOSCDataIndicator =
                                JsonConvert.DeserializeObject<IndicatorULTOSC>(Data);
                            ULTOSCDataIndicator.CleanseData();

                            if (ULTOSCDataIndicator.TechnicalULTOSCdata == null)
                            {
                                failedIndentificator.Add(new IndicatorInfo { Name = ii.Name });

                                continue;
                            }

                            ULTOSCDataIndicator.AddToContext(context);
                        }

                        if (ii.Name == "TRIX")
                        {
                            webR.Url = IndicatorTRIX.GetUrl(ticker.Name);
                            string Data = webR.Download();

                            IndicatorTRIX TRIXDataIndicator = JsonConvert.DeserializeObject<IndicatorTRIX>(Data);
                            TRIXDataIndicator.CleanseData();

                            if (TRIXDataIndicator.TechnicalTRIXdata == null)
                            {
                                failedIndentificator.Add(new IndicatorInfo { Name = ii.Name });

                                continue;
                            }

                            TRIXDataIndicator.AddToContext(context);
                        }

                        if (ii.Name == "DX")
                        {
                            webR.Url = IndicatorDX.GetUrl(ticker.Name);
                            string Data = webR.Download();

                            IndicatorDX DXDataIndicator = JsonConvert.DeserializeObject<IndicatorDX>(Data);
                            DXDataIndicator.CleanseData();

                            if (DXDataIndicator.TechnicalDX == null)
                            {
                                failedIndentificator.Add(new IndicatorInfo { Name = ii.Name });

                                continue;
                            }

                            DXDataIndicator.AddToContext(context);
                        }

                        if (ii.Name == "WILLR")
                        {
                            webR.Url = IndicatorWILLR.GetUrl(ticker.Name);
                            string Data = webR.Download();

                            IndicatorWILLR WILLRDataIndicator = JsonConvert.DeserializeObject<IndicatorWILLR>(Data);
                            WILLRDataIndicator.CleanseData();

                            if (WILLRDataIndicator.TechnicalWILLR == null)
                            {
                                failedIndentificator.Add(new IndicatorInfo { Name = ii.Name });

                                continue;
                            }

                            WILLRDataIndicator.AddToContext(context);
                        }

                        if (ii.Name == "ADXR")
                        {
                            webR.Url = IndicatorADXR.GetUrl(ticker.Name);
                            string Data = webR.Download();

                            IndicatorADXR ADXRDataIndicator = JsonConvert.DeserializeObject<IndicatorADXR>(Data);
                            ADXRDataIndicator.CleanseData();

                            if (ADXRDataIndicator.TechnicalADXR == null)
                            {
                                failedIndentificator.Add(new IndicatorInfo { Name = ii.Name });

                                continue;
                            }

                            ADXRDataIndicator.AddToContext(context);
                        }

                        if (ii.Name == "PPO")
                        {
                            webR.Url = IndicatorPPO.GetUrl(ticker.Name);
                            string Data = webR.Download();

                            IndicatorPPO PPODataIndicator = JsonConvert.DeserializeObject<IndicatorPPO>(Data);
                            PPODataIndicator.CleanseData();

                            if (PPODataIndicator.TechnicalPPO == null)
                            {
                                failedIndentificator.Add(new IndicatorInfo { Name = ii.Name });

                                continue;
                            }

                            PPODataIndicator.AddToContext(context);
                        }

                        if (ii.Name == "MOM")
                        {
                            webR.Url = IndicatorMOM.GetUrl(ticker.Name);
                            string Data = webR.Download();

                            IndicatorMOM MOMDataIndicator = JsonConvert.DeserializeObject<IndicatorMOM>(Data);
                            MOMDataIndicator.CleanseData();

                            if (MOMDataIndicator.TechnicalMOM == null)
                            {
                                failedIndentificator.Add(new IndicatorInfo { Name = ii.Name });

                                continue;
                            }

                            MOMDataIndicator.AddToContext(context);
                        }

                        if (ii.Name == "BOP")
                        {
                            webR.Url = IndicatorBOP.GetUrl(ticker.Name);
                            string Data = webR.Download();

                            IndicatorBOP BOPDataIndicator = JsonConvert.DeserializeObject<IndicatorBOP>(Data);
                            BOPDataIndicator.CleanseData();

                            if (BOPDataIndicator.TechnicalBOP == null)
                            {
                                failedIndentificator.Add(new IndicatorInfo { Name = ii.Name });

                                continue;
                            }

                            BOPDataIndicator.AddToContext(context);
                        }

                        if (ii.Name == "CMO")
                        {
                            webR.Url = IndicatorCMO.GetUrl(ticker.Name);
                            string Data = webR.Download();

                            IndicatorCMO CMODataIndicator = JsonConvert.DeserializeObject<IndicatorCMO>(Data);
                            CMODataIndicator.CleanseData();

                            if (CMODataIndicator.TechnicalCMO == null)
                            {
                                failedIndentificator.Add(new IndicatorInfo { Name = ii.Name });

                                continue;
                            }

                            CMODataIndicator.AddToContext(context);
                        }

                        if (ii.Name == "ROC")
                        {
                            webR.Url = IndicatorROC.GetUrl(ticker.Name);
                            string Data = webR.Download();

                            IndicatorROC ROCDataIndicator = JsonConvert.DeserializeObject<IndicatorROC>(Data);
                            ROCDataIndicator.CleanseData();

                            if (ROCDataIndicator.TechnicalROC == null)
                            {
                                failedIndentificator.Add(new IndicatorInfo { Name = ii.Name });

                                continue;
                            }

                            ROCDataIndicator.AddToContext(context);
                        }

                        if (ii.Name == "PLUS_DI")
                        {
                            webR.Url = IndicatorPlusDi.GetUrl(ticker.Name);
                            string Data = webR.Download();

                            IndicatorPlusDi PlusDiDataIndicator =
                                JsonConvert.DeserializeObject<IndicatorPlusDi>(Data);
                            PlusDiDataIndicator.CleanseData();

                            if (PlusDiDataIndicator.TechnicalPlusDi == null)
                            {
                                failedIndentificator.Add(new IndicatorInfo { Name = ii.Name });

                                continue;
                            }

                            PlusDiDataIndicator.AddToContext(context);
                        }

                        if (ii.Name == "MINUS_DM")
                        {
                            webR.Url = IndicatorMINUS_DM.GetUrl(ticker.Name);
                            string Data = webR.Download();

                            IndicatorMINUS_DM MINUSDMDataIndicator =
                                JsonConvert.DeserializeObject<IndicatorMINUS_DM>(Data);
                            MINUSDMDataIndicator.CleanseData();

                            if (MINUSDMDataIndicator.TechnicalMINUS_DM == null)
                            {
                                failedIndentificator.Add(new IndicatorInfo { Name = ii.Name });

                                continue;
                            }

                            MINUSDMDataIndicator.AddToContext(context);
                        }

                        if (ii.Name == "ADOSC")
                        {
                            webR.Url = IndicatorADOSC.GetUrl(ticker.Name);
                            string Data = webR.Download();

                            IndicatorADOSC ADOSCDataIndicator = JsonConvert.DeserializeObject<IndicatorADOSC>(Data);
                            ADOSCDataIndicator.CleanseData();

                            if (ADOSCDataIndicator.TechnicalADOSC == null)
                            {
                                failedIndentificator.Add(new IndicatorInfo { Name = ii.Name });
                                continue;
                            }

                            ADOSCDataIndicator.AddToContext(context);
                        }

                        if (ii.Name == "HT_TRENDLINE")
                        {
                            webR.Url = IndicatorHT_TRENDLINE.GetUrl(ticker.Name);
                            string Data = webR.Download();

                            IndicatorHT_TRENDLINE HTDataIndicator =
                                JsonConvert.DeserializeObject<IndicatorHT_TRENDLINE>(Data);
                            HTDataIndicator.CleanseData();

                            if (HTDataIndicator.TechnicalHT == null)
                            {
                                failedIndentificator.Add(new IndicatorInfo { Name = ii.Name });

                                continue;
                            }

                            HTDataIndicator.AddToContext(context);
                        }
                    }

                    /*******************************************volume_divided ********************************************************************************/
                    logtoFile.LogMessagePrint(string.Format("Normalize value for{0}", ticker.Name));

                    NormalizeADOSC adosc = new NormalizeADOSC(context);
                    adosc.ADOSCaluculate();

                    NormalizeVolume v = new NormalizeVolume(context); // normalize volume
                    v.VolumeCaluculate();

                    NormalizeVolumeIndex vindex = new NormalizeVolumeIndex(context); // NORMALIZE INDEX VOLUME
                    vindex.VolumeIndexCaluculate();

                    NormalizeAD ad = new NormalizeAD(context);
                    ad.ADCaluculate();

                    NormalizeOBV obv = new NormalizeOBV(context);
                    obv.OBVcalculate();

                    /**********************************************Add day and month************************************************************/
                    logtoFile.LogMessagePrint(string.Format("set seasonal"));
                    SeasonalTime seasonal = new SeasonalTime(context);
                    seasonal.Add_Monts_Day();

                    /**********************************************call formations*****************************************************************************/
                    logtoFile.LogMessagePrint(string.Format("call formation for {0}", ticker.Name));
                    Hammer h = new Hammer(context);
                    h.ProcessHammer();

                    HangedMan hanged = new HangedMan(context);
                    hanged.ProcessHangedMan();

                    Hossa_h hossa_H = new Hossa_h(context);
                    hossa_H.Process_Hossa_h();

                    Bessa_b bessa_b = new Bessa_b(context);
                    bessa_b.Process_Bessa_b();

                    InsideBar ib = new InsideBar(context);
                    ib.Process_InsideBar();

                    DojiStar ds = new DojiStar(context);
                    ds.Process_Doji();

                    MorningStar ms = new MorningStar(context);
                    ms.ProcessMorningStar();

                    EveningStar ev = new EveningStar(context);
                    ev.ProcessEveningStar();

                    /**************************************Currency**************************************************************************************/
                    logtoFile.LogMessagePrint(string.Format("get currency {0}", ticker.Name));
                    // EUR_USD
                    webR.Url = EUR_USD_currency_method.GetUrl();
                    string EUR_USD = webR.Download();

                    EUR_USD_currency_method EUR_USD_CUR =
                        JsonConvert.DeserializeObject<EUR_USD_currency_method>(EUR_USD);
                    EUR_USD_CUR.CleanseData();
                    if (EUR_USD_CUR.TechnicalEUR_USD == null)
                    {
                        throw new NullReferenceException(string.Format("Could not get the currency"));
                    }

                    EUR_USD_CUR.AddToContext(context);

                    //USD_AUD
                    webR.Url = USD_AUD_currency_method.GetUrl();
                    string USD_AUD = webR.Download();

                    USD_AUD_currency_method USD_AUD_CUR =
                        JsonConvert.DeserializeObject<USD_AUD_currency_method>(USD_AUD);
                    USD_AUD_CUR.CleanseData();
                    if (USD_AUD_CUR.TechnicalUSD_AUD == null)
                    {
                        throw new NullReferenceException(string.Format("Could not get the currency"));
                    }

                    USD_AUD_CUR.AddToContext(context);

                    //USD_GBP
                    webR.Url = USD_GBP_currency_method.GetUrl();
                    string USD_GBP = webR.Download();

                    USD_GBP_currency_method USD_GBP_CUR =
                        JsonConvert.DeserializeObject<USD_GBP_currency_method>(USD_GBP);
                    USD_GBP_CUR.CleanseData();
                    if (USD_GBP_CUR.TechnicalUSD_GBP == null)
                    {
                        throw new NullReferenceException(string.Format("Could not get the currency"));
                    }

                    USD_GBP_CUR.AddToContext(context);

                    //USD_JPY
                    webR.Url = USD_JPY_currency_method.GetUrl();
                    string USD_JPY = webR.Download();

                    USD_JPY_currency_method USD_JPY_CUR =
                        JsonConvert.DeserializeObject<USD_JPY_currency_method>(USD_JPY);
                    USD_JPY_CUR.CleanseData();
                    if (USD_JPY_CUR.TechnicalUSD_JPY == null)
                    {
                        throw new NullReferenceException(string.Format("Could not get the currency"));
                    }

                    USD_JPY_CUR.AddToContext(context);

                    /**********************************************save to files***************************************************************************************************************/

                    context.SaveToFile(ticker.Name);
                    context.Save_Reversed(ticker.Name);
                    context.Save_Classifier(ticker.Name);
                }

                return Tuple.Create(failedTickers, failedIndentificator);
            }
            catch (Exception ex)
            {
                LogtoFile logtoFile = new LogtoFile();
                logtoFile.LogMessagePrint(string.Format("Exception occured while running ProcessData: {0}, {1}",
                ex.Message, ex.InnerException));
                return null;
            }
        }
    }
}

