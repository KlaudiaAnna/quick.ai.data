﻿using System;
using System.Collections.Generic;
using QuickAI.ImportData.PrepareDataSet.BLL.Log;
using QuickAI.ImportData.PrepareDataSet.Interfaces.Interfaces;
using QuickIA.ImportData.PrepareDataSet.BLL.Configuration;
namespace QuickAI.ImportData.PrepareDataSet.BLL.GeneralMethods
{
    public class GenerateDataSets : IObtainData
    {
        public bool GetDataSet()
        {
            List<TickersList> t = new List<TickersList>();
            try
            {
                LogtoFile logtoFile = new LogtoFile();

                List<TickerInfo> failedTickers = new List<TickerInfo>();
                List<IndicatorInfo> failedIndentificator = new List<IndicatorInfo>();
                ProccessData process = new ProccessData(); 
                ApplicationConfiguration cfg = Configuration.GetApplicationConfig();

                List<TickerInfo> tickers = cfg.Tickers;
                List<IndicatorInfo> indicators = cfg.Indicators;

                var failed = process.ProcessData(tickers, indicators);
                if (failed != null)
                {
                    var ticker_failed = failed.Item1;

                    var indicators_failded = failed.Item2;

                    if (ticker_failed.Count != 0 && indicators_failded.Count != 0)
                    {
                        logtoFile.LogMessagePrint(string.Format("failed executed its contains {0} and {1} indicators", ticker_failed.Count, indicators_failded.Count));
                        process.ProcessData(ticker_failed, indicators_failded);
                    }

                    if (ticker_failed.Count != 0 && indicators_failded.Count == 0)
                    {
                        logtoFile.LogMessagePrint(string.Format("failed executed its contains {0}", ticker_failed.Count));
                        process.ProcessData(ticker_failed, indicators);
                    }

                    if (ticker_failed.Count == 0 && indicators_failded.Count != 0)
                    {
                        logtoFile.LogMessagePrint(string.Format("failed executed its contains {0}", indicators_failded.Count));
                        process.ProcessData(tickers, indicators_failded);
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                LogtoFile logtoFile = new LogtoFile();
                logtoFile.LogMessagePrint(string.Format("Exception occured while running GetDataSet: {0}, {1}",
                    ex.Message, ex.InnerException));
                return false;
            }
        }
        public List<TickersList> TickerlistList()
        {
            try
            {
                ApplicationConfiguration cfg = Configuration.GetApplicationConfig();
                List<TickersList> t = new List<TickersList>();
                foreach (var tickers in cfg.Tickers)
                {
                    t.Add(new TickersList
                    { 
                        TickerName = tickers.Name,
                        LastOpen = 0,
                        PredictOpen = 0,
                        PredictClose = 0,
                        Teach = false
                    });
                }
                return t;
            }

            catch (Exception ex)
            { 
                LogtoFile logtoFile = new LogtoFile();
                logtoFile.LogMessagePrint(string.Format("Exception occured while running GetDataSet: {0}, {1}",
                ex.Message, ex.InnerException));
                return null;
            }
        }
    }
}



