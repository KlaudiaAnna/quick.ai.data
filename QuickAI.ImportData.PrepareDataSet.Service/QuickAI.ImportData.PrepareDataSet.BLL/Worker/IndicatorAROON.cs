﻿using System;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;
using QuickAI.ImportData.PrepareDataSet.Interfaces.Interfaces;
using QuickIA.ImportData.PrepareDataSet.BLL.Configuration;
using static QuickAI.ImportData.PrepareDataSet.Interfaces.Model.AROON;

namespace QuickAI.ImportData.PrepareDataSet.BLL.Worker
{
    public class IndicatorAROON
    {
        public IndicatorAROON()
        { }
        [JsonProperty(PropertyName = "Meta Data")]
        public MetadataAROON MetadataAROON { get; set; }
        [JsonProperty(PropertyName = "Technical Analysis: AROON")]
        public Dictionary<string, TechnicalAROON> TechnicalAROONdata { get; set; }

        public static string GetUrl (string ticker)
        {
            return string.Concat(Configuration.GetApplicationConfig().ApiUrl, "AROON&symbol=", ticker, "&interval=daily&time_period=10&series_type=close");
        }
        public void AddToContext(Context context)
        {
            foreach (var timeSeria in TechnicalAROONdata)
            {
                ContextElement element = context.GetElementByDate(timeSeria.Key);

                if (element == null)
                {
                    element = new ContextElement
                    {
                        Data = timeSeria.Key
                    };
                    context.Elements.Add(element);
                }
                var values = TechnicalAROONdata[timeSeria.Key];
                element.Values = string.Concat(element.Values, ",", values.AroonDown, ",", values.AroonUp); 
            }
        }
        public void CleanseData()
        {
            DateTime currentDate = DateTime.Now;
            DateTime startDateFromConfig = Configuration.GetApplicationConfig().StartDate;

            int daysToRemove = (currentDate - startDateFromConfig).Days;

            var toRemove = TechnicalAROONdata.Where(x => DateTime.Parse(x.Key) < DateTime.Today.AddDays(-daysToRemove)).ToList();

            for (int i = toRemove.Count - 1; i >= 0; i--)
            {
                TechnicalAROONdata.Remove(toRemove[i].Key);
            }
        }
    }
}
