﻿using System;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;
using QuickAI.ImportData.PrepareDataSet.Interfaces.Interfaces;
using QuickIA.ImportData.PrepareDataSet.BLL.Configuration;
using static QuickAI.ImportData.PrepareDataSet.Interfaces.Model.RSI;

namespace QuickAI.ImportData.PrepareDataSet.BLL.Worker
{
    public class IndicatorRSI
    {
        [JsonProperty(PropertyName = "Meta Data")]
        public MetadataRSI MetadataRsi { get; set; }
        [JsonProperty(PropertyName = "Technical Analysis: RSI")]
        public Dictionary<string, TechnicalRSI> TechnicalRSIdata { get; set; }

        public static string GetUrl(string ticker)
        {
            return string.Concat(Configuration.GetApplicationConfig().ApiUrl,
                "RSI&symbol=", ticker,
                "&interval=daily&time_period=10&series_type=close");
        }
        public void AddToContext(Context context)
        {
            foreach (var timeSeria in TechnicalRSIdata)
            {
                ContextElement element = context.GetElementByDate(timeSeria.Key);

                if (element == null)
                {
                    element = new ContextElement
                    {
                        Data = timeSeria.Key
                    };
                    context.Elements.Add(element);
                }
                var values = TechnicalRSIdata[timeSeria.Key];

                element.Values = string.Concat(element.Values, ",", values.RSI);
            }
        }
        public void CleanseData()
        {
            DateTime currentDate = DateTime.Now;
            DateTime startDateFromConfig = Configuration.GetApplicationConfig().StartDate;

            int daysToRemove = (currentDate - startDateFromConfig).Days;

            var toRemove = TechnicalRSIdata.Where(x => DateTime.Parse(x.Key) < DateTime.Today.AddDays(-daysToRemove)).ToList();

            for (int i = toRemove.Count - 1; i >= 0; i--)
            {
                TechnicalRSIdata.Remove(toRemove[i].Key);
            }
        }
    }
}
