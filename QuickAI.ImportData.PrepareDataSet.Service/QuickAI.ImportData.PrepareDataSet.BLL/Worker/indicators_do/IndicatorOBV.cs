﻿using System;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;
using QuickAI.ImportData.PrepareDataSet.Interfaces.Interfaces;
using QuickIA.ImportData.PrepareDataSet.BLL.Configuration;
using static QuickAI.ImportData.PrepareDataSet.Interfaces.Model.Obv;

namespace QuickAI.ImportData.PrepareDataSet.BLL.Worker
{
    public class IndicatotOBV
    {
        public IndicatotOBV()
        { }
        [JsonProperty(PropertyName = "Meta Data")]
        public MetadataObv MetadataObv { get; set; }
        [JsonProperty(PropertyName = "Technical Analysis: OBV")]
        public  Dictionary<string, TechnicalOBV> TechnicalOBVdata { set; get; }

        public static string GetUrl(string ticker)
        {
            return string.Concat(Configuration.GetApplicationConfig().ApiUrl,
                "OBV&symbol=", ticker,
                "&interval=daily&time_period=10&series_type=close");
        }
        public void AddToContext(Context context)
        {
            foreach (var timeSeria in TechnicalOBVdata)
            {
                ContextElement element = context.GetElementByDate(timeSeria.Key);
                if (element == null)
                {
                    element = new ContextElement
                    {
                        Data = timeSeria.Key
                    };
                    context.Elements.Add(element);
                }
                var values = TechnicalOBVdata[timeSeria.Key];

                System.Globalization.NumberFormatInfo numinf = new System.Globalization.NumberFormatInfo();
                numinf.NumberDecimalSeparator = ".";

                element.OBV = decimal.Parse(values.OBV, numinf);
            }
        }
        public void CleanseData()
        {
            DateTime currentDate = DateTime.Now;
            DateTime startDateFromConfig = Configuration.GetApplicationConfig().StartDate;

            int daysToRemove = (currentDate - startDateFromConfig).Days;

            var toRemove = TechnicalOBVdata.Where(x => DateTime.Parse(x.Key) < DateTime.Today.AddDays(-daysToRemove)).ToList();

            for (int i = toRemove.Count - 1; i >= 0; i--)
            {
                TechnicalOBVdata.Remove(toRemove[i].Key);
            }
        }
    }
}