﻿using System;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;
using QuickAI.ImportData.PrepareDataSet.Interfaces.Interfaces;
using QuickIA.ImportData.PrepareDataSet.BLL.Configuration;
using static QuickAI.ImportData.PrepareDataSet.Interfaces.Model.T3;

namespace QuickAI.ImportData.PrepareDataSet.BLL.Worker
{
    public class IndicatorT3
    {
        public IndicatorT3()
        { } 
        [JsonProperty(PropertyName = "Meta Data")]
        public MetadataT3 MetadataT3 { get; set; }
        [JsonProperty(PropertyName = "Technical Analysis: T3")]
        public Dictionary<string, TechnicalT3> TechnicalT3data { get; set; }

        public static string GetUrl(string ticker) 
        {
            return string.Concat(Configuration.GetApplicationConfig().ApiUrl,"T3&symbol=", ticker,"&interval=daily&time_period=10&series_type=close");
        }
        public void AddToContext(Context context)
        {
            foreach (var timeSeria in TechnicalT3data)
            {
                ContextElement element = context.GetElementByDate(timeSeria.Key);

                if (element == null)
                {
                    element = new ContextElement
                    {
                        Data = timeSeria.Key
                    };
                    context.Elements.Add(element);
                }
                var values = TechnicalT3data[timeSeria.Key];
                element.Values = string.Concat(element.Values, ",", values.T3); 
            }
        }
        public void CleanseData()
        {
            DateTime currentDate = DateTime.Now;
            DateTime startDateFromConfig = Configuration.GetApplicationConfig().StartDate;

            int daysToRemove = (currentDate - startDateFromConfig).Days;

            var toRemove = TechnicalT3data.Where(x => DateTime.Parse(x.Key) < DateTime.Today.AddDays(-daysToRemove)).ToList();

            for (int i = toRemove.Count - 1; i >= 0; i--)
            {
                TechnicalT3data.Remove(toRemove[i].Key);
            }
        }
    }
}
