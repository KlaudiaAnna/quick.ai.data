﻿using System;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;
using QuickAI.ImportData.PrepareDataSet.Interfaces.Interfaces;
using QuickIA.ImportData.PrepareDataSet.BLL.Configuration;
using static QuickAI.ImportData.PrepareDataSet.Interfaces.Model.STOCH;

namespace QuickAI.ImportData.PrepareDataSet.BLL.Worker
{
    public class IndicatorStoch
    {
        public IndicatorStoch()
        { }
        [JsonProperty(PropertyName = "Meta Data")]
        public MetadataSTOCH MetadataSTOCHdata { get; set; }
        [JsonProperty(PropertyName = "Technical Analysis: STOCH")]
        public Dictionary<String, TechnicalSTOCH> TechnicalSTOCHdata { get; set; }

        public static string GetUrl(string ticker)
        {
            return string.Concat(Configuration.GetApplicationConfig().ApiUrl,
                "STOCH&symbol=", ticker,
                "&interval=daily&time_period=10&series_type=close");
        }
        public void AddToContext(Context context)
        {
            foreach (var timeSeria in TechnicalSTOCHdata)
            {
                ContextElement element = context.GetElementByDate(timeSeria.Key);

                if (element == null)
                {
                    element = new ContextElement
                    {
                        Data = timeSeria.Key
                    };
                    context.Elements.Add(element);
                }

                var values = TechnicalSTOCHdata[timeSeria.Key];
                element.Values = string.Concat(element.Values, ",", values.SlowK, ",", values.SlowD);
            }
        }
        public void CleanseData()
        {
            DateTime currentDate = DateTime.Now;
            DateTime startDateFromConfig = Configuration.GetApplicationConfig().StartDate;

            int daysToRemove = (currentDate - startDateFromConfig).Days;

            var toRemove = TechnicalSTOCHdata.Where(x => DateTime.Parse(x.Key) < DateTime.Today.AddDays(-daysToRemove)).ToList();

            for (int i = toRemove.Count - 1; i >= 0; i--)
            {
                TechnicalSTOCHdata.Remove(toRemove[i].Key);
            }
        }
    }
}

