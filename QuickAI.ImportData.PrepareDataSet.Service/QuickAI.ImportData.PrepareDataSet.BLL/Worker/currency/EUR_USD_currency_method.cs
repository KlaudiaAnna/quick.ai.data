﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using QuickAI.ImportData.PrepareDataSet.Interfaces.Interfaces;
using QuickIA.ImportData.PrepareDataSet.BLL.Configuration;
using static QuickAI.ImportData.PrepareDataSet.Interfaces.Model.EUR_USD_currency;

namespace QuickAI.ImportData.PrepareDataSet.BLL.Worker.currency
{
   public class EUR_USD_currency_method
    {
        public  EUR_USD_currency_method()
        { }
        [JsonProperty(PropertyName = "Meta Data")]
        public MetadataEUR_USD MetadataEUR_USD { get; set; }
        [JsonProperty(PropertyName = "Time Series FX (Daily)")]
        public Dictionary<string, TechnicalEUR_USD> TechnicalEUR_USD { get; set; }

        public static string GetUrl()
        {
            return string.Concat(Configuration.GetApplicationConfig().ApiUrl,
                "FX_DAILY&from_symbol=EUR&outputsize=full&to_symbol=USD");
        }
        public void AddToContext(Context context)
        {
            foreach (var timeSeria in TechnicalEUR_USD)
            {
                ContextElement element = context.GetElementByDate(timeSeria.Key);

                if (element == null)
                {
                    element = new ContextElement
                    {
                        Data = timeSeria.Key
                    };
                    context.Elements.Add(element);
                }
                var values = TechnicalEUR_USD[timeSeria.Key];
                element.Values = String.Concat(element.Values,",",values.CloseEUR_USD);
            }
        }

        public void CleanseData()
        {
            DateTime currentDate = DateTime.Now;
            DateTime startDateFromConfig = Configuration.GetApplicationConfig().StartDate;

            int daysToRemove = (currentDate - startDateFromConfig).Days;

            var toRemove = TechnicalEUR_USD.Where(x => DateTime.Parse(x.Key) < DateTime.Today.AddDays(-daysToRemove)).ToList();

            for (int i = toRemove.Count - 1; i >= 0; i--)
            {
                TechnicalEUR_USD.Remove(toRemove[i].Key);
            }
        }
    }
}
