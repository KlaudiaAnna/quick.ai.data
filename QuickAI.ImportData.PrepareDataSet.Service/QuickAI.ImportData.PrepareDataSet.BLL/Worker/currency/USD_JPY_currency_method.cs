﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using QuickAI.ImportData.PrepareDataSet.Interfaces.Interfaces;
using QuickIA.ImportData.PrepareDataSet.BLL.Configuration;
using static QuickAI.ImportData.PrepareDataSet.Interfaces.Model.USD_JPY_currency;

namespace QuickAI.ImportData.PrepareDataSet.BLL.Worker.currency
{
    public class USD_JPY_currency_method
    {
        public USD_JPY_currency_method()
        { }
        [JsonProperty(PropertyName = "Meta Data")]
        public MetadataUSD_JPY MetadataUSD_JPY { get; set; }
        [JsonProperty(PropertyName = "Time Series FX (Daily)")]
        public Dictionary<string, TechnicalUSD_JPY> TechnicalUSD_JPY { get; set; }

        public static string GetUrl()
        {
            return string.Concat(Configuration.GetApplicationConfig().ApiUrl,
                "FX_DAILY&from_symbol=USD&outputsize=full&to_symbol=GBP");
        }
        public void AddToContext(Context context)
        {
            foreach (var timeSeria in TechnicalUSD_JPY)
            {
                ContextElement element = context.GetElementByDate(timeSeria.Key);

                if (element == null)
                {
                    element = new ContextElement
                    {
                        Data = timeSeria.Key
                    };
                    context.Elements.Add(element);
                }
                var values = TechnicalUSD_JPY[timeSeria.Key];

                element.Values = String.Concat(element.Values, ",", values.CloseUSD_JPY);
            }
        }

        public void CleanseData()
        {
            DateTime currentDate = DateTime.Now;
            DateTime startDateFromConfig = Configuration.GetApplicationConfig().StartDate;

            int daysToRemove = (currentDate - startDateFromConfig).Days;

            var toRemove = TechnicalUSD_JPY.Where(x => DateTime.Parse(x.Key) < DateTime.Today.AddDays(-daysToRemove)).ToList();

            for (int i = toRemove.Count - 1; i >= 0; i--)
            {
                TechnicalUSD_JPY.Remove(toRemove[i].Key);
            }
        }


    }
}
