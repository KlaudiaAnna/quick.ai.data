﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using QuickAI.ImportData.PrepareDataSet.Interfaces.Interfaces;
using QuickIA.ImportData.PrepareDataSet.BLL.Configuration;
using static QuickAI.ImportData.PrepareDataSet.Interfaces.Model.USD_GBP_currency;

namespace QuickAI.ImportData.PrepareDataSet.BLL.Worker.currency
{
    public class USD_GBP_currency_method
    {
        public USD_GBP_currency_method()
        { }
        [JsonProperty(PropertyName = "Meta Data")]
        public MetadataUSD_GBP MetadataUSD_GBP { get; set; }
        [JsonProperty(PropertyName = "Time Series FX (Daily)")]
        public Dictionary<string, TechnicalUSD_GBP> TechnicalUSD_GBP { get; set; }

        public static string GetUrl()
        {
            return string.Concat(Configuration.GetApplicationConfig().ApiUrl,
                "FX_DAILY&from_symbol=USD&outputsize=full&to_symbol=GBP");
        }
        public void AddToContext(Context context)
        {
            foreach (var timeSeria in TechnicalUSD_GBP)
            {
                ContextElement element = context.GetElementByDate(timeSeria.Key);

                if (element == null)
                {
                    element = new ContextElement
                    {
                        Data = timeSeria.Key
                    };
                    context.Elements.Add(element);
                }
                var values = TechnicalUSD_GBP[timeSeria.Key];

                element.Values = String.Concat(element.Values,",", values.CloseUSD_GBP);
            }
        }

        public void CleanseData()
        {
            DateTime currentDate = DateTime.Now;
            DateTime startDateFromConfig = Configuration.GetApplicationConfig().StartDate;

            int daysToRemove = (currentDate - startDateFromConfig).Days;

            var toRemove = TechnicalUSD_GBP.Where(x => DateTime.Parse(x.Key) < DateTime.Today.AddDays(-daysToRemove)).ToList();

            for (int i = toRemove.Count - 1; i >= 0; i--)
            {
                TechnicalUSD_GBP.Remove(toRemove[i].Key);
            }
        }


    }
}
