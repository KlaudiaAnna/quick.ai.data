﻿using System;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;
using QuickAI.ImportData.PrepareDataSet.Interfaces.Interfaces;
using QuickIA.ImportData.PrepareDataSet.BLL.Configuration;
using static QuickAI.ImportData.PrepareDataSet.Interfaces.Model.MACD;

namespace QuickAI.ImportData.PrepareDataSet.BLL.Worker
{
    public class IndicatorMACD
    {
        public IndicatorMACD()
        { }
        [JsonProperty(PropertyName = "Meta Data")]
        public MetadataMACD MetadataMACD { get; set; }
        [JsonProperty(PropertyName = "Technical Analysis: MACD")]
        public Dictionary<String, TechnicalMACD> TechnicalMACDdata { get; set; }

        public static string GetUrl(string ticker)
        {
            return string.Concat(Configuration.GetApplicationConfig().ApiUrl,
                "MACD&symbol=", ticker,
                "&interval=daily&time_period=10&series_type=close");
        }
        public void AddToContext(Context context)
        {
            foreach (var timeSeria in TechnicalMACDdata)
            {
                ContextElement element = context.GetElementByDate(timeSeria.Key);
                if (element == null)
                {
                    element = new ContextElement
                    {
                        Data = timeSeria.Key
                    };
                    context.Elements.Add(element);
                }
                var values = TechnicalMACDdata[timeSeria.Key];
                element.Values = string.Concat(element.Values, ",", values.MACD, ",", values.MACD_Hist, ",", values.MACD_Signal);
                
            }
        }
        public void CleanseData()
        {

            DateTime currentDate = DateTime.Now;
            DateTime startDateFromConfig = Configuration.GetApplicationConfig().StartDate;

            int daysToRemove = (currentDate - startDateFromConfig).Days;

            var toRemove = TechnicalMACDdata.Where(x => DateTime.Parse(x.Key) < DateTime.Today.AddDays(-daysToRemove)).ToList();

            for (int i = toRemove.Count - 1; i >= 0; i--)
            {
                TechnicalMACDdata.Remove(toRemove[i].Key);
            }
        }
    }
}
