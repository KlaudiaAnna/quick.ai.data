﻿using System;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;
using QuickAI.ImportData.PrepareDataSet.Interfaces.Interfaces;
using QuickIA.ImportData.PrepareDataSet.BLL.Configuration;
using static QuickAI.ImportData.PrepareDataSet.Interfaces.Model.PLUS_DI;

namespace QuickAI.ImportData.PrepareDataSet.BLL.Worker
{
    public class IndicatorPlusDi
    {
        public IndicatorPlusDi()
        { }
        [JsonProperty(PropertyName = "Meta Data")]
        public Metadata Metadata { get; set; }
        [JsonProperty(PropertyName = "Technical Analysis: PLUS_DI")]
        public Dictionary<string, Technical> TechnicalPlusDi { get; set; }

        public static string GetUrl(string ticker)
        {
            return string.Concat(Configuration.GetApplicationConfig().ApiUrl,
                "PLUS_DI&symbol=", ticker,
                "&interval=daily&time_period=10&series_type=close");
        }
        public void AddToContext(Context context)
        {
            foreach (var timeSeria in TechnicalPlusDi)
            {
                ContextElement element = context.GetElementByDate(timeSeria.Key);

                if (element == null)
                {
                    element = new ContextElement
                    {
                        Data = timeSeria.Key
                    };
                    context.Elements.Add(element);
                }
                var values = TechnicalPlusDi[timeSeria.Key];
                element.Values = string.Concat(element.Values, ",", values.PLUS_DI);

            }
        }
        public void CleanseData()
        {
            DateTime currentDate = DateTime.Now;
            DateTime startDateFromConfig = Configuration.GetApplicationConfig().StartDate;

            int daysToRemove = (currentDate - startDateFromConfig).Days;

            var toRemove = TechnicalPlusDi.Where(x => DateTime.Parse(x.Key) < DateTime.Today.AddDays(-daysToRemove)).ToList();

            for (int i = toRemove.Count - 1; i >= 0; i--)
            {
                TechnicalPlusDi.Remove(toRemove[i].Key);
            }
        }
    }
}
