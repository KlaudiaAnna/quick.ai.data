﻿using System;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;
using QuickAI.ImportData.PrepareDataSet.Interfaces.Interfaces;
using QuickIA.ImportData.PrepareDataSet.BLL.Configuration;
using static QuickAI.ImportData.PrepareDataSet.Interfaces.Model.CMO;

namespace QuickAI.ImportData.PrepareDataSet.BLL.Worker
{
    public class IndicatorCMO
    {
        public IndicatorCMO()
        { }
        [JsonProperty(PropertyName = "Meta Data")]
        public Metadata Metadata { get; set; }
        [JsonProperty(PropertyName = "Technical Analysis: CMO")]
        public Dictionary<string, Technical> TechnicalCMO{ get; set; }

        public static string GetUrl(string ticker)
        {
            return string.Concat(Configuration.GetApplicationConfig().ApiUrl,
                "CMO&symbol=", ticker,
                "&interval=daily&time_period=10&series_type=close");
        }
        public void AddToContext(Context context)
        {
            foreach (var timeSeria in TechnicalCMO)
            {
                ContextElement element = context.GetElementByDate(timeSeria.Key);

                if (element == null)
                {
                    element = new ContextElement
                    {
                        Data = timeSeria.Key
                    };
                    context.Elements.Add(element);
                }
                var values = TechnicalCMO[timeSeria.Key];
                element.Values = string.Concat(element.Values, ",", values.CMO);

            }
        }
        public void CleanseData()
        {
            DateTime currentDate = DateTime.Now;
            DateTime startDateFromConfig = Configuration.GetApplicationConfig().StartDate;

            int daysToRemove = (currentDate - startDateFromConfig).Days;

            var toRemove = TechnicalCMO.Where(x => DateTime.Parse(x.Key) < DateTime.Today.AddDays(-daysToRemove)).ToList();

            for (int i = toRemove.Count - 1; i >= 0; i--)
            {
                TechnicalCMO.Remove(toRemove[i].Key);
            }
        }
    }
}
