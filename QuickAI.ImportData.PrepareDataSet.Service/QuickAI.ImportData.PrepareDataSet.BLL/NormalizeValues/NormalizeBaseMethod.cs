﻿
using QuickAI.ImportData.PrepareDataSet.Interfaces.Interfaces;
using QuickAI.ImportData.PrepareDataSet.Interfaces.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace QuickAI.ImportData.PrepareDataSet.BLL.NormalizeValues
{
    public class MyPoint
    {
        public Context context;
        public MyPoint(Context c)
        {
            context = c;

        }



        public void Methodreturns(ContextElement x)
        {
   
            var valueMax = context.Elements.Max(element => element.x);
            for (int i = 0; i < context.Elements.Count; i++)
            {
                decimal value = context.Elements[i].x;
                value = value / valueMax;
                var el = context.Elements[i];
                string tostring = value.ToString();
                var indicatornormalized = tostring.Replace(",", ".");
                el.Values = string.Concat(el.Values, ",", indicatornormalized);
            }
        }

        
    }
}
