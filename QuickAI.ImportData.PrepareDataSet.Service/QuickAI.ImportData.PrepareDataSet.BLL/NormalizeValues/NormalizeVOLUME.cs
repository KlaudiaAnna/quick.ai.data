﻿using QuickIA.ImportData.PrepareDataSet.BLL;

using System.Linq;
namespace QuickAI.ImportData.PrepareDataSet.BLL.NormalizeValues
{
    class NormalizeVolume
    {
        public Context context;
        public NormalizeVolume(Context c)
        {
            context = c;
        }
        /* volume */

        public void VolumeCaluculate()
        {
            var volumeMax = context.Elements.Max(element => element.Volume); // find the max value of volume

            for (int i = 0; i < context.Elements.Count; i++)
            {
                decimal volume = context.Elements[i].Volume;
                volume = volume / volumeMax;
                var el = context.Elements[i];
                string volumestring = volume.ToString();
                var vol = volumestring.Replace(",", ".");
                el.Values = string.Concat(el.Values, ",", vol);
            }
        }
    }
}