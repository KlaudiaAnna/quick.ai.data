﻿using System;
using System.Globalization;
using System.ServiceProcess;
using System.Threading;
using QuickAI_WindowsService.Learning;
using QuickAI_WindowsService.SerializeDeserializeJson;
using System.Collections.Generic;
using QuickAI_WindowsService.ObtainServices;

namespace QuickAI_WindowsService
{
    public partial class QuickAI : ServiceBase
    {
        Thread t;
        private ManualResetEvent shutdown;
        public QuickAI()
        {
            shutdown = new ManualResetEvent(false);
            t = null;
            InitializeComponent();
        }

        internal void Start(string[] args)
        {
            OnStart(args);
        }
        protected override void OnStart(string[] args)
        {
            base.OnStart(args);
            t = new Thread(DoWork);

            t.Start();
        }
        private void DoWork()
        {
            try
            {
                Log.LogMessage(" Get configuration from app.config");
                Serialize jSave = new Serialize();
                var (m_config, h_config, count, h1, m1, Day1, Day2) = DateParametr.GetParamtrTime();
                while (!shutdown.WaitOne(0))
                {
                    Log.LogMessage("Check time");
                    if ((DateTime.Now.Hour == h_config) && ((DateTime.Now.Hour + 4 >= h_config) && (DateTime.Now.DayOfWeek >= Day1) && (DateTime.Now.DayOfWeek <= Day2)))
                    {
                        var load = Deserialize.LoadJson("State.Json");

                        load.ForEach(x =>
                       {
                           if (x.Date.Day != DateTime.Now.Day || load.Count == 0)

                               Log.LogMessage("Call method GetDataSet()");
                        ObtainDataClient cl = new ObtainDataClient();
                        //  bool result = cl.ReturnedTrueorFalse();
                        bool result = true;

                        Log.LogMessage(string.Format("method GetDataSet() returned {0} ", result));

                        DataMongoClient c2l = new DataMongoClient();
                        // bool resultdatabase = c2l.SetsDatabse(); 
                        bool resultdatabase = true;

                        Log.LogMessage(string.Format("method GetDataSet() returned {0} ", resultdatabase));

                        if (result && resultdatabase)
                        {
                            Log.LogMessage("Send email ");
                            MailUtils.SendEmail("QuickAI Windows Services - was successed",
                            String.Format("Function returned: {0}", true));
                        }
                        if (!result || !resultdatabase)
                        {
                            Log.LogMessage("Send email ");
                            MailUtils.SendEmail("QuickAI Windows Services - was NOT successed",
                            String.Format("Function {0}", false));
                        }
                        jSave.SetToJsonObtain(result, resultdatabase);

                        if ((DateTime.Now.Hour == h1) && (Day2 == DateTime.Now.DayOfWeek))
                        {
                            var loadlearning = Deserialize.LoadJson("StateofLearning.Json");

                            load.ForEach(z =>
                            {
                                if (z.ObtainData == true)
                                    loadlearning.ForEach(y =>  { if (y.Date != DateTime.Now.Date)

                                        Log.LogMessage("Started Teaching");
                                        List<TickersList> list = Teach.SpecifyState();

                                        jSave.SetToJsonTikersList(list);
                                        List<TickersList> after_predict = Predict.PredictState(list);
                                        jSave.SetToJsonTikersList(after_predict);
                                        Save.SaveProcess(after_predict);
                                    });
                            });
                        }
                    });
                    }
                    Log.LogMessage("Go to sleep");
                    Thread.Sleep(58000);
                    Thread.Sleep(3000);
                }
            }
            catch (Exception ex)
            {
                Log.LogMessage(string.Format("Exception occured while running : {0}, {1}", ex.Message, ex.InnerException));
                MailUtils.SendEmail("QuickAI Windows Services - Fatal Exception",
                 string.Format("Exception occured while running : {0}, {1}", ex.Message, ex.InnerException));
            }
        }
        protected override void OnStop()
        {
            base.OnStop();
            try
            {
                Log.LogMessage("Stop Services " + DateTime.Now.ToString(CultureInfo.InvariantCulture));
                t.Abort();
                Log.LogMessage("Has stopped? " + t.IsAlive);
            }
            catch (Exception ex)
            {
                Log.LogMessage(string.Format("Exception occured while running : {0}, {1}", ex.Message, ex.InnerException));
                Log.LogMessage(ex.Message);
                if (ex.InnerException != null) Log.LogMessage(ex.InnerException.ToString());
            }
        }
    }
}

