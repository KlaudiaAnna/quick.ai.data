﻿using System;
namespace QuickAI_WindowsService
{
    public static class IdGenarator
    {
        public static string GenerateID()
        {
            return Guid.NewGuid().ToString("N");
        }
    }
}
