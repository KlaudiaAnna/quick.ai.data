﻿using QuickAI_WindowsService.ObtainServices;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
namespace QuickAI_WindowsService
{
    public static class Save
    {
        public static void SaveProcess(List<TickersList> list)
        {
            var path = ConfigurationManager.AppSettings["State_Tickers"];

            var fileCombine = Path.Combine(Environment.CurrentDirectory, path);

            if (File.Exists(fileCombine))
            {
                File.Delete(fileCombine);
            }

            using (StreamWriter writetext = new StreamWriter(fileCombine + "State" + ".csv", true))
            {
                foreach (var l in list)
                {
                    writetext.WriteLine(DateTime.Now + ", " + l.TickerName + ", "+ l.Teach +  ", last open price: " + l.LastOpen + ", predict open price: " + l.PredictOpen + ", last close price:" + l.PredictClose);
                }
            }
        }
    }
}



