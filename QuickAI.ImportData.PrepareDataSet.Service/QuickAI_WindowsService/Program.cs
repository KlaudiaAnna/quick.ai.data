﻿using System;
using System.ServiceProcess;
namespace QuickAI_WindowsService
{
    static class Program
    {
        static void Main()
        {
            QuickAI srv = new QuickAI();
            if(Environment.UserInteractive)
            {
                Console.WriteLine("Start service");
                srv.Start(null);
                Console.WriteLine("Started");
            }
            else
            {
                ServiceBase[] ServicesToRun;
                ServicesToRun = new ServiceBase[]
                {
                    srv
                };
                ServiceBase.Run(ServicesToRun);
            }
        }
    }
}
