﻿using System;
using System.Configuration;
using System.IO;
using System.Web;
using System.Web.Hosting;

namespace QuickAI_WindowsService
{
    public static class Log
    {

        public static void LogMessage(string message)
        {
            try
            {
                string path = Path.Combine(Environment.CurrentDirectory, ConfigurationManager.AppSettings["log_path"]);

                StreamWriter sw = new StreamWriter(path + "log_" + DateTime.Now.ToShortDateString() + ".txt", true);

                sw.WriteLine(DateTime.Now + "  " + message);
                sw.Close();
            }

            catch { LogMessageError(message); }
        }

        public static void LogMessageError(string message)
        {
            try
            {
                string path = ConfigurationManager.AppSettings["log_path"];
                StreamWriter sw = new StreamWriter(path + "log_" + DateTime.Now.ToShortDateString() + "_Catch.txt", true);
                sw.WriteLine(" " + message);
                sw.Close();
            }
            catch { }
        }
    }
}
