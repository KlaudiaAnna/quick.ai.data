﻿using System;
using System.Collections.Generic;
using System.IO;
using Newtonsoft.Json;
using QuickAI_WindowsService.Model;
namespace QuickAI_WindowsService.SerializeDeserializeJson
{
    public static class Deserialize
    {
        public static List<WroteTickers> LoadJson(string filename)
        {
            string s = Path.Combine(Environment.CurrentDirectory, @"C:\Projects\Quick.AI.Data\QuickIA.ImportData.DataSet\QuickAI.ImportData.PrepareDataSet.Service\QuickAI_WindowsService\Json\");
            string checkifexist = s + filename;

            using (StreamReader read = new StreamReader(s + filename))
            {
                if (File.Exists(checkifexist))
                {
                    string json = read.ReadToEnd();
                    List<WroteTickers> GetList = JsonConvert.DeserializeObject<List<WroteTickers>>(json);

                    return GetList;
                }
                return null;
            }
        }
    }
}
