﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using QuickAI_WindowsService.ObtainServices;
using QuickAI_WindowsService.Model;
using System.IO;
using System.Security.Cryptography.X509Certificates;
namespace QuickAI_WindowsService.SerializeDeserializeJson
{
    public class Serialize
    {
        List<WroteTickers> wroteTickers = new List<WroteTickers>();

        readonly string save = Path.Combine(Environment.CurrentDirectory, @"C:\Projects\Quick.AI.Data\QuickIA.ImportData.DataSet\QuickAI.ImportData.PrepareDataSet.Service\QuickAI_WindowsService\Json\");
        JsonSerializer serializer = new JsonSerializer();
    
        public void SetToJsonObtain(bool obtain, bool mongo)
        {
            using (StreamWriter file = new StreamWriter(save + "State" + ".Json"))
            {
                var id = IdGenarator.GenerateID();
                wroteTickers.Add(new WroteTickers()
                {
                    Date = DateTime.Now,
                    Id = id,
                    ObtainData = obtain,
                    DataMongo = mongo
                });
                serializer.Serialize(file, wroteTickers);
            }
        }
        public void SetToJsonTikersList(List<TickersList> tickers)
        {
            foreach (var el in tickers)
            {
                using (StreamWriter file = new StreamWriter(save + "StateofLearning" + ".Json"))
                {
                    var id = IdGenarator.GenerateID();
                    wroteTickers.Add(new WroteTickers()
                    {
                        Date = DateTime.Now,
                        Id = id,
                        Ticker = el.TickerName,
                        Teach = el.Teach
                    });
                    serializer.Serialize(file, wroteTickers);
                }
            }
        }
    }
}

