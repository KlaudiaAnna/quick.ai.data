﻿using System;
using System.Configuration;
namespace QuickAI_WindowsService
{
    public abstract class DateParametr
    {
        public static (int m_config, int h_config, Int32 count, int h1, int m1, DayOfWeek Day1, DayOfWeek Day2) GetParamtrTime()
        {
            string counter = ConfigurationManager.AppSettings["Counter"];
            var count = Int32.Parse(counter);
            string startTime = ConfigurationManager.AppSettings["StartTime"];
            DateTime time = DateTime.Parse(startTime);
            int hConfig = time.Hour;
            int mConfig = time.Minute;
            string startTeach = ConfigurationManager.AppSettings["StartTeach"];
            DateTime timetTime = DateTime.Parse(startTeach);
            int hour = timetTime.Hour;
            int minut = timetTime.Minute;

            string Monday = ConfigurationManager.AppSettings["Monday"];
            string Friday = ConfigurationManager.AppSettings["Friday"];
            DayOfWeek Monday_conf = (DayOfWeek)Enum.Parse(typeof(DayOfWeek), Monday);
            DayOfWeek Friday_conf = (DayOfWeek)Enum.Parse(typeof(DayOfWeek), Friday);

            return (mConfig, hConfig, count, hour, minut, Monday_conf, Friday_conf);
        }
    }
}
