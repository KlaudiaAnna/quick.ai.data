﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace QuickAI_WindowsService.Model
{
    public class WroteTickers
    {
        [JsonProperty(PropertyName = "Id")]
        public string Id { get; set; }
        [JsonProperty(PropertyName = "Date")]
        public DateTime Date { get; set; }
        [JsonProperty(PropertyName = "Ticker")]
        public string Ticker { get; set; }
        [JsonProperty(PropertyName = "Teach")]
        public bool Teach { get; set; }
        [JsonProperty(PropertyName = "Price_1")]
        public decimal Price_1 { get; set; }
        [JsonProperty(PropertyName = "Price_2")]
        public decimal Price_2 { get; set; }
        [JsonProperty(PropertyName = "Price_3")]
        public decimal Price_3 { get; set; }
        [JsonProperty(PropertyName = "ObtainData")]
        public bool ObtainData { get; set; }
        [JsonProperty(PropertyName = "DataMongo")]
        public bool DataMongo { get; set; }
    }
}
