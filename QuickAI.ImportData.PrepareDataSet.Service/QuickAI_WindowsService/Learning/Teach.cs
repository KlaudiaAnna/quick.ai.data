﻿using QuickAI_WindowsService.LearningManager;
using QuickAI_WindowsService.ObtainServices;
using System;
using System.Collections.Generic;
namespace QuickAI_WindowsService.Learning
{
    public static class Teach
    {
        public static List<TickersList> SpecifyState()
        {
            try
            {
                QuickAIManagerClient clienttAiManagerClient = new QuickAIManagerClient();
                ObtainDataClient client = new ObtainDataClient();
                var listTickers = client.TickerswithOpenCloseBol();
                List<TickersList> list = new List<TickersList>();

            //    foreach (var ticker in listTickers)
              //  {
                    Log.LogMessage(string.Format("try teach for ticker {0}", "AAPL"));

                    bool learn = clienttAiManagerClient.TeachTheModel("AAPL");

                    list.Add(new TickersList
                    {
                        Teach = learn,
                        TickerName = "AAPL"
                    });
           //     }
                return list;

            }
            catch (Exception ex)
            {
                Log.LogMessage(string.Format("Exception occured while running GetDataSet: {0}, {1}", ex.Message, ex.InnerException));
                return null;
            }
        }
    }
}
