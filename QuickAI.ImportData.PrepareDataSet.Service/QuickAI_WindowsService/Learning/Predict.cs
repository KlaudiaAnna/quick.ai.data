﻿using QuickAI_WindowsService.LearningManager;
using QuickAI_WindowsService.ObtainServices;
using System.Collections.Generic;
using System.Linq;
namespace QuickAI_WindowsService
{
    public static class Predict
    {
        public static List<TickersList>  PredictState(List<TickersList> list)
        {
            QuickAIManagerClient quickAI = new QuickAIManagerClient();

            List<TickersList> predictlist = new List<TickersList>();

            foreach (var el in list)
            {
                if (el.Teach == true)
                {
                    Log.LogMessage(string.Format("execute predict for ticker {0}",el.TickerName));

                    decimal[] predict =  quickAI.Predict(el.TickerName);

                    predictlist.Add(new TickersList
                    {
                        TickerName = el.TickerName,
                        LastOpen = predict.ElementAt(0),
                        PredictOpen = predict.ElementAt(1),
                        PredictClose = predict.ElementAt(2)

                    });

                }
            }
            return predictlist;
        }
    }
}
