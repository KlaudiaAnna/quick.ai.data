﻿using QuickAI.ImportData.PrepareDataSet.TestHarness.Obtain_Test;
using System;
using System.Windows.Forms;
namespace QuickAI.ImportData.PrepareDataSet.TestHarness
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        private void BtnRun_Click(object sender, EventArgs e)
        {
            try
            {
                ObtainDataClient client = new ObtainDataClient();
                txtResult.Text = client.ReturnedTrueorFalse().ToString();

                //DataMongoClient dataMongo = new DataMongoClient();
                //txtResult.Text = dataMongo.SetsDatabse().ToString();



            }
            catch (Exception ex)
            {
                txtLog.Text = ex.ToString();
                txtLog.Text += ex.InnerException;
            }
        }
    }
}
