﻿using Newtonsoft.Json;
using System.Collections.Generic;
namespace QuickAI.ImportData.PrepareDataSet.Interfaces.TwitterModel
{
    public class Tweetdata
    {
        [JsonProperty(PropertyName = "statuses")]
        public List<Tweet> Statuses { get; set; }
    }
}

