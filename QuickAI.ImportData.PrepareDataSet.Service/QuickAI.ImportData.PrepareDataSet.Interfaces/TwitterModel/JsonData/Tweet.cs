﻿using Newtonsoft.Json;
namespace TwitterTest.JsonData
{
    public class Tweet
    {
        public Tweet()
        { }
        [JsonProperty(PropertyName = "created_at")]
        public string Created { get; set; }
        [JsonProperty(PropertyName = "id_str")]
        public string Id_str { get; set; }
        [JsonProperty(PropertyName = "text")]
        public string Text { get; set;}
        [JsonProperty(PropertyName = "user")]
        public UserObject User { get; set; }
        [JsonProperty(PropertyName = "entities")]
        public Entities Entities { get; set; }
    }
}