﻿using Newtonsoft.Json;
namespace TwitterTest.JsonData
{
    public class UserObject
    {
        public UserObject()
        { }
        [JsonProperty(PropertyName = "id")]
        public string IDuser { get; set; }
        [JsonProperty(PropertyName = "name")]
        public string Name { get; set; }                                                                     
    }
}
