﻿using Newtonsoft.Json;

namespace TwitterTest.JsonData
{
    public class Symbols
    {
        public Symbols()
        { }
        [JsonProperty(PropertyName = "text")]
        public string Text { get; set; }
        [JsonProperty(PropertyName = "indices")]
        public int[] Indices { get; set; }
    }
}
