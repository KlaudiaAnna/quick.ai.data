﻿using System;
using Newtonsoft.Json;
namespace QuickAI.ImportData.PrepareDataSet.Interfaces.TwitterModel
{
    public class Tweet
    {
        [JsonProperty(PropertyName = "created_at")]
        public string Created { get; set; }
        [JsonProperty(PropertyName = "id_str")]
        public Int64 Id_str { get; set; }
        [JsonProperty(PropertyName = "full_text")]
        public string Text { get; set;}
        [JsonProperty(PropertyName = "user")]
        public UserObject User { get; set; }
        [JsonProperty(PropertyName = "entities")]
        public Entities Entities { get; set; }
        [JsonProperty(PropertyName = "retweeted_status")]
        public Retweed Retweeted { get; set; }
    }
}