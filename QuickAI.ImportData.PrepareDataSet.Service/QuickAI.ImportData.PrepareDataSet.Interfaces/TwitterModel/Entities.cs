﻿using System.Collections.Generic;
using Newtonsoft.Json;
namespace QuickAI.ImportData.PrepareDataSet.Interfaces.TwitterModel
{
    public class Entities
    {
        [JsonProperty(PropertyName = "symbols")]
        public List<Symbols> Symbols { get; set; }
    }
}