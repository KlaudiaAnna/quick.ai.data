﻿using Newtonsoft.Json;

namespace QuickAI.ImportData.PrepareDataSet.Interfaces.Model
{
    public class Ema
    {
        public Ema()
        { }

        public class MetadataEma
        {
            public MetadataEma()
            { }
            [JsonProperty(PropertyName = "1: Symbol")]
            public string SymbolEma { get; set; }
            [JsonProperty(PropertyName = "2: Indicator")]
            public string IndicatorEma { get; set; }
            [JsonProperty(PropertyName = "3: Last Refreshed")]
            public string LastRefreshedEma { get; set; }
            [JsonProperty(PropertyName = "4: Interval")]
            public string IntervalEma { get; set; }
            [JsonProperty(PropertyName = "5: Time Period")]
            public string TimePeriodEma { get; set; }
            [JsonProperty(PropertyName = "6: Series Type")]
            public string SeriesTypeEma { get; set; }
            [JsonProperty(PropertyName = "7: Time Zone")]
            public string TimeZoneEma { get; set; }
        }
        public class TechnicalEMA
        {
            public TechnicalEMA()
            { }
            [JsonProperty(PropertyName = "EMA")]
            public string EMA { get; set; }
        }
    }
}