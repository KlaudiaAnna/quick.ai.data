﻿using Newtonsoft.Json;

namespace QuickAI.ImportData.PrepareDataSet.Interfaces.Model
{
    public class CCI
    {
        public CCI()
        { }

        public class MetadataCCI
        {
            public MetadataCCI()
            { }
            [JsonProperty(PropertyName = "1: Symbol")]
            public string SymbolCCI { get; set; }
            [JsonProperty(PropertyName = "2: Indicator")]
            public string IndicatorCCI { get; set; }
            [JsonProperty(PropertyName = "3: Last Refreshed")]
            public string LastRefreshedCCI { get; set; }
            [JsonProperty(PropertyName = "4: Interval")]
            public string IntervalCCI { get; set; }
            [JsonProperty(PropertyName = "5: Time Period")]
            public string TimePeriodCCI { get; set; }
            [JsonProperty(PropertyName = "6: Time Zone")]
            public string TimeZoneCCI { get; set; }
        }
        public class TechnicalCCI
        {
            public TechnicalCCI()
            { }
            [JsonProperty(PropertyName = "CCI")]
            public string CCI { get; set; }
        }
    }
}