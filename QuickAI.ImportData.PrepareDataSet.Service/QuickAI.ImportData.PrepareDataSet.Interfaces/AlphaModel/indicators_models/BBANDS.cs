﻿using Newtonsoft.Json;

namespace QuickAI.ImportData.PrepareDataSet.Interfaces.Model
{
    public class BBANDS
    {
        public BBANDS()
        { }

        public class MetadataBBANDS
        {
            public MetadataBBANDS()
            { }
            [JsonProperty(PropertyName = "1: Symbol")]
            public string SymbolBBANDS { get; set; }
            [JsonProperty(PropertyName = "2: Indicator")]
            public string IndicatorBBANDS { get; set; }
            [JsonProperty(PropertyName = "3: Last Refreshed")]
            public string LastRefreshedBBANDS { get; set; }
            [JsonProperty(PropertyName = "4: Interval")]
            public string IntervalBBANDS { get; set; }
            [JsonProperty(PropertyName = "5: Time Zone")]
            public string TimePeriodBBANDS { get; set; }
            [JsonProperty(PropertyName = "6.1: Deviation multiplier for upper band")]
            public string UpperBandBBANDS { get; set; }
            [JsonProperty(PropertyName = "6.2: Deviation multiplier for lower band")]
            public string LowerBandBBANDS { get; set; }
            [JsonProperty(PropertyName = "6.3: MA Type")]
            public string MAtypeBBANDS { get; set; }
            [JsonProperty(PropertyName = "7: Series Type")]
            public string SeriesTypeBBANDS { get; set; }
            [JsonProperty(PropertyName = "8: Time Zone")]
            public string TimeZoneBBANDS { get; set; }
        }
        public class TechnicalBBANDS
        {
            public TechnicalBBANDS()
            { }
            [JsonProperty(PropertyName = "Real Middle Band")]
            public string RealMidddleBands { get; set; }
            [JsonProperty(PropertyName = "Real Lower Band")]
            public string RealLowerBands { get; set; }
            [JsonProperty(PropertyName = "Real Upper Band")]
            public string RealUpperBands { get; set; }
        }
    }
}
