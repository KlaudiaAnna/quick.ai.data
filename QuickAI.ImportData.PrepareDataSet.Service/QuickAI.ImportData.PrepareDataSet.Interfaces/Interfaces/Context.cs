﻿
namespace QuickAI.ImportData.PrepareDataSet.Interfaces.Interfaces
{
    public class ContextElement
    {
        public string Data { get; set; }
        public string Values { get; set; }

        public decimal Open { get; set; }
        public decimal Close { get; set; }
        public decimal High { get; set; }
        public decimal Low { get; set; }
        public decimal Volume { get; set; }

        public decimal OpenIndex { get; set; }
        public decimal CloseIndex { get; set; }
        public decimal HighIndex { get; set; }
        public decimal LowIndex { get; set; }
        public decimal VolumeIndex { get; set; }

        public decimal AD { get; set; }
        public decimal OBV { get; set; }
        public decimal ADOSC { get; set; }
        

    }
}
