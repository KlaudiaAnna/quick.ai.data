﻿using Newtonsoft.Json;
namespace QuickAI.ImportData.PrepareDataSet.Interfaces.StockTwitsModel
{
    public class Symbol
    {
        [JsonProperty(PropertyName = "symbol")]
        public string Symbolticker { get; set; }
    }
}