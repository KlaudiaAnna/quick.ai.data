﻿
using Newtonsoft.Json;

namespace QuickAI.ImportData.PrepareDataSet.Interfaces.Model
{
    public class RSI
    {
        public RSI()
        { }

        public class MetadataRSI
        {
            public MetadataRSI()
            { }
            [JsonProperty(PropertyName = "1: Symbol")]
            public string SymbolRSI { get; set; }
            [JsonProperty(PropertyName = "2: Indicator")]
            public string IndicatorRSI { get; set; }
            [JsonProperty(PropertyName = "3: Last Refreshed")]
            public string LastRefreshedRSI { get; set; }
            [JsonProperty(PropertyName = "4: Interval")]
            public string IntervalRSI { get; set; }
            [JsonProperty(PropertyName = "5: Time Period")]
            public string TimePeriodORSI { get; set; }
            [JsonProperty(PropertyName = "6: Series Type")]
            public string TSeriesTypeRSI { get; set; }
            [JsonProperty(PropertyName = "7: Time Zone")]
            public string TimeZoneRSI { get; set; }
        }
        public class TechnicalRSI
        {
            public TechnicalRSI()
            { }
            [JsonProperty(PropertyName = "RSI")]
            public string RSI { get; set; }
        }
    }
}
