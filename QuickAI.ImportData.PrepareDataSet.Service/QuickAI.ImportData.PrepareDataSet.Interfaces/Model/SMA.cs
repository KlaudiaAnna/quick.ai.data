﻿using Newtonsoft.Json;

namespace QuickAI.ImportData.PrepareDataSet.Interfaces.Model
{
    public class SMA
    {

        public SMA()
        { }

        public class MetadataSMA
        {
            public MetadataSMA()
            { }
            [JsonProperty(PropertyName = "1: Symbol")]
            public string SymbolSMA { get; set; }
            [JsonProperty(PropertyName = "2: Indicator")]
            public string IndicatorSMA { get; set; }
            [JsonProperty(PropertyName = "3: Last Refreshed")]
            public string LastRefreshedSMA { get; set; }
            [JsonProperty(PropertyName = "4: Interval")]
            public string IntervalSMA { get; set; }
            [JsonProperty(PropertyName = "5: Time Period")]
            public string TimePeriodSMA { get; set; }
            [JsonProperty(PropertyName = "6: Series Type")]
            public string TSeriesTypeSMA { get; set; }
            [JsonProperty(PropertyName = "7: Time Zone")]
            public string TimeZoneSMA { get; set; }
        }
        public class TechnicalSMA
        {
            public TechnicalSMA()
            { }
            [JsonProperty(PropertyName = "SMA")]
            public string SMA { get; set; }
        }
    }
}

