﻿using Newtonsoft.Json;

namespace QuickAI.ImportData.PrepareDataSet.Interfaces.Model
{
    public class MACD
    {
        public MACD()
        { }

        public class MetadataMACD
        {
            public MetadataMACD()
            { }
            [JsonProperty(PropertyName = "1: Symbol")]
            public string SymbolMACD { get; set; }
            [JsonProperty(PropertyName = "2: Indicator")]
            public string IndicatorMACD { get; set; }
            [JsonProperty(PropertyName = "3: Last Refreshed")]
            public string LastRefreshedMACD { get; set; }
            [JsonProperty(PropertyName = "4: Interval")]
            public string IntervalMACD { get; set; }
            [JsonProperty(PropertyName = "5.1: Fast Period")]
            public string FastPeriodMACD { get; set; }
            [JsonProperty(PropertyName = "5.2: Slow Period")]
            public string SlowPeriodMACD { get; set; }
            [JsonProperty(PropertyName = "5.3: Signal Period")]
            public string SignalPeriodMACD { get; set; }
            [JsonProperty(PropertyName = "6: Series Type")]
            public string SeriesTypeMACD { get; set; }
            [JsonProperty(PropertyName = "7: Time Zone")]
            public string TimeZoneMACD { get; set; }
        }
        public class TechnicalMACD
        {
            public TechnicalMACD()
            { }
            [JsonProperty(PropertyName = "MACD_HIST")]
            public string MACD_Hist { get; set; }
            [JsonProperty(PropertyName = "MACD")]
            public string MACD { get; set; }
            [JsonProperty(PropertyName = "MACD_Signal")]
            public string MACD_Signal { get; set; }
        }
    }
}
