﻿using Newtonsoft.Json;

namespace QuickAI.ImportData.PrepareDataSet.Interfaces.Model
{
    public class ADX
    {
        public ADX()
        { }

        public class MetadataADX
        {
            public MetadataADX()
            { }
            [JsonProperty(PropertyName = "1: Symbol")]
            public string SymbolADX { get; set; }
            [JsonProperty(PropertyName = "2: Indicator")]
            public string IndicatorADX { get; set; }
            [JsonProperty(PropertyName = "3: Last Refreshed")]
            public string LastRefreshedADX { get; set; }
            [JsonProperty(PropertyName = "4: Interval")]
            public string IntervalADX { get; set; }
            [JsonProperty(PropertyName = "5: Time Period")]
            public string TimePeriodADX { get; set; }
            [JsonProperty(PropertyName = "6: Time Zone")]
            public string TimeZoneADX { get; set; }
        }

        public class TechnicalADX
        {
            public TechnicalADX()
            { }
            [JsonProperty(PropertyName = "ADX")]
            public string ADX { get; set; }
        }
    }
}

