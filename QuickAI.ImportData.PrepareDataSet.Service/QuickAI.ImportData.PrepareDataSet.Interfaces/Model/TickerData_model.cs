﻿using Newtonsoft.Json;

namespace QuickAI.ImportData.PrepareDataSet.Interfaces.Model
{
    public class TickerData_model
    {
        public TickerData_model()
        { }

        public class Metadata
        {
            public Metadata()
            { }
            [JsonProperty(PropertyName = "1. Information")]
            public string Information { get; set; }
            [JsonProperty(PropertyName = "2. Symbol")]
            public string Symbol { get; set; }
            [JsonProperty(PropertyName = "3. Last Refreshed")]
            public string LastRefreshed { get; set; }
            [JsonProperty(PropertyName = "4. Output Size")]
            public string OutputSize { get; set; }
            [JsonProperty(PropertyName = "5. Time Zone")]
            public string TimeZone { get; set; }
        }
        public class TimeSeries
        {
            public TimeSeries()
            { }
            [JsonProperty(PropertyName = "1. open")]
            public string Open { get; set; }
            [JsonProperty(PropertyName = "2. high")]
            public string High { get; set; }
            [JsonProperty(PropertyName = "3. low")]
            public string Low { get; set; }
            [JsonProperty(PropertyName = "4. close")]
            public string Close { get; set; }
            [JsonProperty(PropertyName = "5. volume")]
            public string Volume { get; set; }
        }
    }
}