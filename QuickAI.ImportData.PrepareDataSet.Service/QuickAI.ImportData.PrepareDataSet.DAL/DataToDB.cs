﻿using System;
using System.Collections.Generic;
using QuickAI.ImportData.PrepareDataSet.Interfaces.Interfaces;
namespace QuickAI.ImportData.PrepareDataSet.DAL
{
    public class DataToDb : IDataMongo
    {
        public List<TweetCollectionCompleted> list;
        public DataToDb(List<TweetCollectionCompleted> list)
        {
            this.list = list;
        }
        public bool SetData()
        {
            try
            {
                Insert insert = new Insert();
                insert.InsertToCollection(list);
                return true;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return false;
            }
        }   
    }     
}
